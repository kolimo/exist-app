$(document).ready(function() {
    // make .Titel editable first, hidden rows are not going to be modifided
    // $('.titel, .erscheinungsjahr, .kategorie, .unterkategorie, .literatur, .kommentar').editable();

    $('#gesamtliste').DataTable( {
        "serverSide": true,
        "ajax": "json/gesamtliste.json"
    } );
    
// 	var table = $('#gesamtliste').dataTable({
// 		'info': false,
// 		'sDom': 'lTfr<"clearfix">tip',
// 		'oTableTools': {
//             'aButtons': [
//                 {
//                     'sExtends':    'collection',
//                     'sButtonText': '<i class="fa fa-cloud-download"></i>&nbsp;&nbsp;&nbsp;<i class="fa fa-caret-down"></i>',
//                     'aButtons':    [ 'csv', 'xls', 'pdf', 'copy', 'print' ]
//                 }
//             ]
//         }
// 	});
	
$('#gesamtliste').on( 'draw.dt', function () {
    $('.makeediticon').click(function(){
        $(this).parent().children('.cell').editable();
    });
} );
	
	
	// new $.fn.dataTable.FixedHeader( table );
});