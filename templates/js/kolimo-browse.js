$(document).ready( function(){
console.log("KOLIMO browse module");
function listener(id){
    // cleanup
    $("#"+id+" input").val("");
    
    // listener
    $("#"+id+" .selector > li").click(function() {
        var id=$(this).parents('.criteria').attr('id'); // c0
        var text=$(this).children('a').text();
        var input=$(this).data('input');
        var old=$("#"+id+" .selection").text();
        var oldInput=$("#"+id+" li:contains('"+old+"')").data('input');

        // rewrite .selection
        $("#"+id+" .selection").text( text );
        // remove old input field
        $("#"+id+" ."+oldInput).toggle();
        $("#"+id+" ."+oldInput).attr("data-name", $(this).attr('name')).removeAttr('name');
        
        var name=$("#"+id+" ."+input).data('name');
        $("#"+id+" ."+input).attr('name', name);
        $("#"+id+" ."+input).toggle();
    });
}

$( "#add" ).click(function(e) {
    var newId= 'c'+$('.criteria').length;
   $( '.criteria' ).clone().first().prop('id', newId).insertBefore('#add');
    listener(newId);
});
listener('c0');
$(".DateInput").mask("9999-9999");
});