var app = new annotator.App();

app.include(annotator.ui.main, {
    element: document.querySelector('.main-box-body'),
    editorExtensions: [annotator.ui.tags.editorExtension],
    viewerExtensions: [annotator.ui.tags.viewerExtension]
});

app.include(annotator.storage.http, {
    prefix: '/annotator',
    urls: {
        create: '/modules/create.xql',
        update: '/annotations/{id}',
        destroy: '/modules/delete.xql?id={id}',
        search: '/modules/search.xql'
        }
});

app
    .start()
    .then(function () {
        app.annotations.load();
    });