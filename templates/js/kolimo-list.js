$(document).ready(function() {
    $('#gesamtliste').DataTable( {
        "serverSide": true,
        "ajax": "json/list.json",
        "lengthMenu": [[100, 250, 500, -1], [100, 250, 500, "All"]]
    } );
	
    $('#gesamtliste').on( 'draw.dt', function () {
        $('.makeediticon').click(function(){
            $(this).parent().children('.cell').editable();
        });
    } );
	
});