(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};

		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);

			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;

			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};

			$self.data('countTo', data);

			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);

			// initialize the element with the starting value
			render(value);

			function updateTimer() {
				value += increment;
				loopCount++;

				render(value);

				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}

				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;

					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}

			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.text(formattedValue);
			}
		});
	};

	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};

	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
	

function labelFormatter(label, series) {
    return "<div style='font-size:150%; text-align:center; padding:10px; color:white;'>" + label + "<br/>" + series.data[0][1] + "</div>";
}

// Authors in KOLIMO
        var dataDonut = [
            { label: "Gutenberg",  data: 1267},
            { label: "TextGrid",  data:376},
            { label: "DTA",  data: 633}
        ];

		
		$.plot('#graph-flot-donut-authors', dataDonut, {
		    series: {
		        pie: {
		            show: true,
		            radius: 500,
		            label: {
		                show: true,
		                formatter: labelFormatter,
		                background: {
                            opacity: 0.5,
                            color: '#000'
		                }
		            }
		        }
		    },
			colors: ['#e74c3c', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
		    legend: {
		        show: false,
		    }
		});
// Titles in KOLIMO
        var dataDonut = [
            { label: "Gutenberg",  data: 13985},
            { label: "TextGrid",  data: 27410},
            { label: "DTA",  data: 1304}
        ];
		
		$.plot('#graph-flot-donut-titles', dataDonut, {
		    series: {
		        pie: {
		            show: true,
		            radius: 500,
		            label: {
		                show: true,
		                formatter: labelFormatter,
		                background: {
                            opacity: 0.5,
                            color: '#000'
		                }
		            }
		        }
		    },
			colors: ['#e74c3c', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
		    legend: {
		        show: false,
		    }
		});

// Dates in the Metadata
    var db1 = [ 
        [1007,1],[1149,1],[1195,1],[1210,1],[1233,1],[1253,1],[1263,1],[1303,1],[1354,1],[1382,1],[1384,1],[1392,1],[1433,1],[1435,1],[1456,1],[1484,1],[1499,1],[1500,2],[1503,1],[1508,1],[1509,1],[1513,1],[1516,1],[1517,1],[1522,1],[1525,1],[1529,1],[1531,2],[1532,1],[1533,3],[1534,2],[1536,1],[1539,2],[1540,1],[1544,1],[1545,1],[1546,1],[1549,1],[1551,1],[1553,1],[1557,1],[1558,1],[1561,1],[1562,1],[1571,2],[1573,2],[1576,1],[1578,1],[1580,1],[1584,1],[1588,1],[1589,1],[1590,1],[1592,1],[1594,1],[1598,2],[1600,2],[1609,1],[1611,2],[1612,1],[1614,2],[1618,2],[1619,1],[1620,1],[1621,1],[1622,1],[1623,1],[1624,2],[1625,3],[1628,3],[1630,1],[1632,1],[1633,2],[1635,2],[1637,2],[1639,2],[1640,1],[1642,2],[1643,2],[1645,5],[1646,1],[1647,2],[1649,1],[1650,1],[1651,1],[1653,1],[1654,2],[1655,2],[1657,2],[1658,2],[1659,2],[1661,2],[1662,1],[1663,2],[1664,3],[1665,2],[1666,3],[1668,1],[1669,3],[1670,1],[1671,3],[1673,2],[1675,2],[1676,1],[1677,1],[1678,1],[1679,2],[1680,4],[1681,1],[1683,3],[1684,1],[1685,2],[1686,2],[1687,2],[1688,1],[1690,3],[1691,1],[1692,2],[1693,1],[1694,1],[1697,2],[1699,2],[1700,1],[1701,3],[1702,4],[1703,2],[1705,1],[1706,1],[1708,1],[1710,2],[1711,3],[1712,1],[1715,1],[1716,1],[1718,1],[1722,2],[1723,1],[1724,1],[1725,1],[1727,1],[1728,1],[1729,1],[1730,2],[1731,1],[1732,1],[1733,3],[1735,2],[1736,1],[1739,2],[1740,5],[1741,4],[1743,2],[1744,2],[1745,5],[1746,4],[1747,2],[1748,3],[1749,2],[1751,5],[1752,2],[1753,5],[1754,1],[1755,2],[1756,3],[1758,4],[1759,5],[1760,2],[1761,3],[1762,2],[1763,1],[1764,1],[1765,1],[1766,2],[1767,4],[1768,5],[1769,3],[1771,4],[1772,4],[1773,4],[1774,5],[1775,2],[1776,8],[1777,2],[1778,2],[1779,3],[1780,6],[1781,5],[1782,9],[1783,3],[1784,6],[1785,6],[1786,2],[1787,3],[1788,4],[1789,5],[1790,3],[1791,2],[1792,4],[1793,3],[1794,1],[1795,6],[1796,10],[1797,5],[1798,4],[1799,2],[1800,4],[1801,3],[1802,7],[1803,7],[1804,6],[1805,6],[1806,6],[1807,3],[1808,4],[1809,9],[1810,6],[1811,8],[1812,9],[1813,2],[1814,8],[1815,2],[1816,15],[1817,7],[1818,4],[1819,3],[1820,5],[1821,4],[1822,7],[1823,9],[1824,3],[1825,8],[1826,3],[1827,3],[1828,5],[1829,4],[1830,9],[1831,3],[1832,8],[1833,11],[1834,6],[1835,4],[1836,8],[1837,6],[1838,9],[1839,6],[1840,7],[1841,8],[1842,6],[1843,7],[1844,6],[1845,7],[1846,8],[1847,11],[1848,10],[1849,10],[1850,8],[1851,10],[1852,12],[1853,12],[1854,10],[1855,14],[1856,11],[1857,10],[1858,7],[1859,14],[1860,9],[1861,15],[1862,3],[1863,9],[1864,9],[1865,10],[1866,12],[1867,12],[1868,11],[1869,8],[1870,10],[1871,4],[1872,7],[1873,10],[1874,13],[1875,10],[1876,11],[1877,9],[1878,8],[1879,10],[1880,12],[1881,9],[1882,17],[1883,10],[1884,12],[1885,11],[1886,15],[1887,21],[1888,11],[1889,14],[1890,5],[1891,11],[1892,18],[1893,12],[1894,9],[1895,17],[1896,16],[1897,15],[1898,15],[1899,10],[1900,13],[1901,15],[1902,15],[1903,28],[1904,13],[1905,23],[1906,12],[1907,15],[1908,16],[1909,17],[1910,25],[1911,21],[1912,24],[1913,18],[1914,17],[1915,16],[1916,14],[1917,13],[1918,9],[1919,8],[1920,7],[1921,10],[1922,6],[1923,7],[1924,8],[1925,5],[1926,6],[1927,3],[1928,1],[1930,2],[1931,2],[1933,1],[1934,2],[1935,1],[1936,1],[1939,2],[1940,1],[1941,1],[1944,1],[1945,2],[1948,1],[1952,1],[1954,1],[1956,2],[1957,1],[1958,1],[1968,1],[1979,1]
        ];
	
	var series = new Array();

	series.push({
		data : db1,
		bars : {
			show : true,
			barWidth : 0.2,
			order : 1,
			lineWidth: 1,
			fill: 1
		}
	});

	$.plot("#graph-bar", series, {
		colors: ['#e74c3c', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
		grid: {
			tickColor: "#ddd",
			borderWidth: 0,
		yaxis: { ticks: [0.001,0.01,0.1,1,10,100],
                 transform:  function(v) {return Math.log(v+0.0001); /*move away from zero*/} , tickDecimals: 3 }
		},
		shadowSize: 0
	});
	
}(jQuery));

$('.infographic-box .value .timer').countTo({});