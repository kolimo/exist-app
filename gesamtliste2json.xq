xquery version "3.1";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "json";
declare option output:media-type "application/json";

let $table := 'gesamtliste.xml'
let $doc := doc('/db/apps/kolimo/data/'||$table)
let $unusedNames := ('InKafkasBibliothek', 'Standort', 'InLiterarischesUmfeld', 'InKafkasBibliothek', 'Expertenrating', 'edit')
let $heads := distinct-values(($doc/*/entry)[1]/*[not(local-name() = $unusedNames)]/local-name())

return
(
"{ 'draw': 1, 'recordsTotal': 57, 'recordsFiltered': 57, 'data': [",
    string-join((for $entry at $pos in $doc//entry
        where $pos lt 3
        return
        string-join(("[",
        string-join(
            (for $item at $posi in $entry/*
            where $item/not(local-name() = $unusedNames)
                return
                '"'|| $item/string() || '"'
        ), ','),
    	"]"
        ), '')
    ), ',')
, "]}")
