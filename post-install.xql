xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare variable $target external;
let $thisCollection := if(ends-with($target, '/'))
    then string-join( tokenize( $target, '/' )[position() lt last() - 1] , '/')
    else string-join( tokenize( $target, '/' )[position() != last()] , '/')
return(

try { sm:create-group('kolimouser', 'kolimo user group') } catch * { <error/> },
try { sm:create-group('kolimoadmin', 'kolimo admin group') } catch * { <error/> },
try { sm:create-account('kolimouser', 'kolimodefault', 'kolimouser') } catch * { <error/> },
try { sm:create-account('kolimoadmin', 'kolimodefault', ('kolimouser', 'kolimoadmin')) } catch * { <error/> },

for $i in ('berenike', 'markus', 'maxi', 'gerhard', 'gabriela', 'mathias')
    return
    (
        try {sm:create-account($i, 'kolimo'||$i, ('kolimouser', 'kolimoadmin')) } catch * { <error/> },
        if(sm:group-exists($i)) then sm:remove-group($i) else true()
    ),

sm:chmod(		xs:anyURI( $target || '/templates/json/list.json'), 'rwxr-xr-x'),
xmldb:set-mime-type(	xs:anyURI( $target || '/templates/json/list.json'), 'application/xquery'),

(: increase security by moving the securitywrapper to a collection not exposed by the proxy:
 : the wrapper wont be accessible via port 443
 : calls via localhost:8080  or any other port configured:)

xmldb:move($target, $thisCollection, 'securitywrapper.xql')
)
