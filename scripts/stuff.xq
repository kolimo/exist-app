xquery version "3.1";
(:~
 : @author Maxi Wess
 :)

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare default element namespace "http://www.tei-c.org/ns/1.0";


declare function local:load_id_date_pairs(){
    let $source := util:binary-to-string(util:binary-doc('/db/tmp/kolimo_gender_dofp.tsv'))
    return
        for $line in tokenize($source, '\n')
        let $tokens := tokenize($line, '\t')
(:  :        let $book_id := tokenize($line, ';')[1]
        let $gnd := tokenize($line, )
        let $year := tokenize($line, ';')[3] :)
        return
            if ((compare($tokens[3], 'nan') eq 0) or (compare($tokens[4], 'nan') eq 0)) then (
                (($tokens)))
            else ()
};

declare function local:get_author($doc){
    let $surname := $doc//tei:teiHeader/fileDesc/titleStmt/author/persName/surname/text()
    let $forename := $doc//tei:teiHeader/fileDesc/titleStmt/author/persName/forename/text()
    return
        string-join(($surname, $forename), ", ")
};

declare function local:get_gnd($doc){
    let $s := string($doc//teiHeader/fileDesc/titleStmt/author/persName/@ref)
    return
        tokenize($s, '/')[last()]
};
(:
let $doc := doc('/db/data/kolimo/sources/dta/kafka_urteil_1913.TEI-P5.xml')
return
    string($doc//teiHeader/fileDesc/titleStmt/author/persName/@ref)
:)
let $kgf := local:load_id_date_pairs()
let $collection := collection('/db/data/kolimo/header')
let $text := for $doc in $collection
    return
        let $kid := $doc//tei:fileDesc/tei:publicationStmt/tei:idno[@type='kolimo']/text()
        let $author := local:get_author($doc)
        let $gnd := local:get_gnd($doc)
        return
            if ($kid = $kgf) then (string-join(($kid, $author, $gnd), codepoints-to-string(9))) else ()
    return
        xmldb:store('/db/tmp/', 'liste', string-join($text, codepoints-to-string(10)))
