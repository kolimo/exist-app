xquery version "3.1";
(:~
 : @author Maxi Wess
 :)

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare default element namespace "http://www.tei-c.org/ns/1.0";


declare function local:find_entry($col, $book_id as xs:string){
    for $doc in $col
        let $idno := $doc/tei:teiHeader/fileDesc/publicationStmt/idno[@type="kolimo"]
        return
            if (compare($book_id, $idno) eq 0) then (
                $doc)
            else ()
};

declare function local:db_pairs(){
    let $collection_dta := collection('/db/data/kolimo/header/dta')
    let $collection_gb := collection('/db/data/kolimo/header/gb')
    let $collection_tg := collection('/db/data/kolimo/header/tg')
    let $collections := ($collection_dta, $collection_gb, $collection_tg)
    (: let $collections := collection("/db/test_data") :)
    return
        $collections
        (:($collections//tei:teiHeader/fileDesc/publicationStmt/idno[@type="kolimo"]/text(), $collections//tei:teiHeader/fileDesc/notesStmt) :)
        (: ($collections//tei:teiHeader/fileDesc/publicationStmt/idno[@type="kolimo"]/text(), $collections//tei:teiHeader/profileDesc/textClass/keywords[@scheme="http://kolimo.uni-goettingen.de/metadata#literature-non-literature"]/list/item) :)
};


declare function local:load_id_date_pairs(){
    let $source := util:binary-to-string(util:binary-doc('/db/tmp/kolimo_gender_dofp.tsv'))
    return
        for $line in tokenize($source, '\n')
        let $tokens := tokenize($line, '\t')
(:  :       let $book_id := tokenize($line, ';')[1]
        let $gnd := tokenize($line, )
        let $year := tokenize($line, ';')[3] :)
        return
            $tokens[0]
            (:if ((compare($tokens[3], 'nan') eq 0) or (compare($tokens[4], 'nan') eq 0)) then (
                ())
            else ($tokens[1]) :)
};


declare function local:find_unfinished_nodes(){
    let $ids := local:load_id_date_pairs()
    let $col := local:db_pairs()
    let $allnodes := $col//tei:teiHeader/fileDesc/notesStmt
        return
            for $node in $allnodes
                return
                    if ((compare($node/note[@type='kolimo-date'][1]/text(), 'nan') eq 0) or
                        (compare($node/note[@type='author-gender'][1]/text(), 'nan') eq 0) or
                        (compare($node/note[@type='kolimo-date'][1]/text(), '') eq 0) or
                        (compare($node/note[@type='author-gender'][1]/text(), '') eq 0)
                    )then ($node/ancestor::fileDesc)
                    else (())
};

declare function local:has_kolimo_date($doc){
    let $node := $doc//tei:teiHeader/fileDesc/notesStmt/note[@type='kolimo-date']
    return
        if (compare($node/text(), '') eq 0) then(True cast as xs:boolean)
        else()
};

(:
declare function local:check_node_completion($node){
            if ((compare($node/note[@type='kolimo-date'][1], 'nan') eq 0) or
                (compare($node/note[@type='author-gender'][1], 'nan') eq 0) or
                (compare($node/note[@type='kolimo-date'][1]/text(), '') eq 0) or
                (compare($node/note[@type='author-gender'][1]/text(), '') eq 0)
            )
            then (True cast as xs:boolean)
            else (False cast as xs:boolean)
};
:)

declare function local:unfinished_nodes_kolimo_id(){
    let $nodes := local:find_unfinished_nodes()
    return
        $nodes/publicationStmt/idno[@type="kolimo"]/text()
};

(:
let $n := local:unfinished_nodes_kolimo_id()
let $file_nodes := local:load_id_date_pairs()
return
    fn:distinct-values($n[.=$file_nodes])
:)


(:
let $kids := local:unfinished_nodes_kolimo_id()
return
    xmldb:store("/db/tmp", "unfinished_nodes_kids.txt", string-join($kids, '!!!???!!!'))
:)

declare function local:get_date($node){
    if (boolean($node//tei:teiHeader/fileDesc/publicationStmt/date[@type="publication"])) then
        ($node//tei:teiHeader/fileDesc/publicationStmt/date[@type="publication"]/text())
        else(
            if (boolean($node//tei:teiHeader/fileDesc/notesStmt/note[@type="kolimo-date"])) then
                ($node//tei:teiHeader/fileDesc/notesStmt/note[@type="kolimo"]/text())
                else
                    (-1)
            )
};

declare function local:get_tabular_data_dta($doc){
    let $title := local:get_title($doc)
    let $author := local:get_author($doc)
    let $genre := local:get_genre_dta($doc)
    let $dop_date := local:get_dop($doc)
    let $kolimo_date := local:get_kolimo_date($doc)
    let $litornot := local:get_literature($doc)
    return
        ($title, $author, $genre, $dop_date, $kolimo_date, $litornot)
};

declare function local:get_tabular_data_gb($doc){
    let $title := local:get_title($doc)
    let $author := local:get_author($doc)
    let $genre := local:get_genre_gb($doc)
    let $dop_date := local:get_dop($doc)
    let $kolimo_date := local:get_kolimo_date($doc)
    let $litornot := local:get_literature($doc)
    return
        ($title, $author, $genre, $dop_date, $kolimo_date, $litornot)
};

declare function local:get_tabular_data_tg($doc){
    let $title := local:get_title($doc)
    let $author := local:get_author($doc)
    let $genre := local:get_genre_tg($doc)
    let $dop_date := local:get_dop($doc)
    let $kolimo_date := local:get_kolimo_date($doc)
    let $litornot := local:get_literature($doc)
    return
        ($title, $author, $genre, $dop_date, $kolimo_date, $litornot)
};

declare function local:get_literature($doc){
    if (boolean($doc//tei:teiHeader/profileDesc/textClass/keywords[@scheme="http://kolimo.uni-goettingen.de/metadata#literature-non-literature"]/list/item))
    then ($doc//tei:teiHeader/profileDesc/textClass/keywords[@scheme="http://kolimo.uni-goettingen.de/metadata#literature-non-literature"]/list/item/text())
    else ("NA")
};

declare function local:extract_date($node as xs:string){
    $node
};

declare function local:get_dop($doc){
    if (boolean($doc//tei:teiHeader/profileDesc/creation/date[@type="firstPublication"])) then
        ($doc//tei:teiHeader/profileDesc/creation/date[@type="firstPublication"]/text())
    else ("NA")
};

declare function local:get_kolimo_date($doc){
    if (boolean($doc//tei:teiHeader/fileDesc/notesStmt/note[@type="kolimo-date"][1])) then
                ($doc//tei:teiHeader/fileDesc/notesStmt/note[@type="kolimo-date"][1]/text())
    else ("NA")
};

declare function local:get_genre_dta($doc){
    let $genre_main := local:get_main_genre_dta($doc)
    let $genre_sub := local:get_sub_genre_dta($doc)
        return ($genre_main, $genre_sub)
};

declare function local:get_main_genre_dta($doc){
   let $genre_nodes_main := $doc//tei:teiHeader/profileDesc/textClass/keywords[@scheme="http://www.deutschestextarchiv.de/doku/klassifikation#dwds1Kategorie"]/list/item[1]
   return
       if (boolean($genre_nodes_main)) then ($genre_nodes_main/text()) else ("NA")
};

declare function local:get_sub_genre_dta($doc){
    let $genre_nodes_sub := $doc//tei:teiHeader/profileDesc/textClass/keywords[@scheme="http://www.deutschestextarchiv.de/doku/klassifikation#dwds1Unterkategorie"]/list/item[1]
    return
        if (boolean($genre_nodes_sub)) then ($genre_nodes_sub/text()) else ("NA")
};

declare function local:get_genre_gb($doc){
    let $genre := $doc//tei:teiHeader/profileDesc/textClass/keywords[@scheme="http://gutenberg.spiegel.de/genre/"]/list/item[1]
    return
        if (boolean($genre)) then (
        $genre/text()) else ("NA")
};

declare function local:get_genre_tg($doc){
    let $genre := $doc//tei:teiHeader/profileDesc/textClassscheme[@type="http://textgrid.info/namespaces/metadata/core/2010#genre"]/term
    return
        $genre/text()
};

declare function local:get_author1($doc){
    string-join($doc//tei:teiHeader/fileDesc/titleStmt/author/persName/*/text(),  ", ")
};

declare function local:get_author($doc){
    let $surname := $doc//tei:teiHeader/fileDesc/titleStmt/author/persName/surname/text()
    let $forename := $doc//tei:teiHeader/fileDesc/titleStmt/author/persName/forename/text()
    return
        string-join(($surname, $forename), ", ")
};

declare function local:get_title($doc){
    if (boolean($doc//tei:teiHeader/fileDesc/titleStmt/title[@type="main"])) then
        ($doc//tei:teiHeader/fileDesc/titleStmt/title[@type="main"]/text())
    else ("NA")
};

declare function local:get_text($doc){
    string($doc//TEI/text)
};

declare function local:extract_text($doc){
    string-join($doc//text(), codepoints-to-string(10))
};

declare function local:get_text_gb($doc){
    string-join($doc//body//text(), codepoints-to-string(10))
};

declare function local:get_text_tg($doc){
    string-join($doc//text//text(), codepoints-to-string(10))
};


(: for $doc in collection('/db/data/kolimo/header/tg') : :)
let $doc := doc('/db/data/kolimo/header/tg/1001-Altenberg__Peter.xml')
return
    let $srcpath := document-uri($doc)
    let $fname := tokenize($srcpath, '/')[last()]
    let $dstpath := replace($srcpath, '/db/data/kolimo/header', '/tmp/merging')
    let $sourcespath := replace($srcpath, '/db/data/kolimo/header', '/db/data/kolimo/sources')
    let $cleartext_document := doc($sourcespath)
    let $dstdir := replace($dstpath, $fname, '')
    let $cleartexts := local:get_text_tg($cleartext_document)
    let $header := $doc//tei:teiHeader[1]
    let $newheader := <TEI>{$header, <text>{$cleartexts}</text>}</TEI>
    return
        file:serialize($newheader, $dstpath, '')

(:
declare function local:get_conversion($doc){
    let $path := replace(document-uri($doc), 'header', 'sources')
    let $doc_source := doc($path)
    let $fname := replace(tokenize(document-uri($doc), '/')[last()], '.xml', '.txt')
    let $full_text := local:get_text_gb($doc_source)
    let $title := string-join(('TITLE: ', local:get_title($doc)), '')
    let $author := string-join(('AUTHOR: ', local:get_author($doc)),'')
    let $kolimo_date := string-join(('KOLIMO DATE: ', local:get_kolimo_date($doc)), '')
    let $dop := string-join(('DATE OF PUBLICATION: ', local:get_dop($doc)), '')
    let $output := string-join(($title, $author, $kolimo_date, $dop), codepoints-to-string(10))
    return
        (xmldb:store('/db/tmp/trilcke/meta/gb', $fname, $output), xmldb:store('/db/tmp/trilcke/fulltext/gb', $fname, $full_text))
};


declare function local:check_for_valid_name($doc){
    try{
        string($doc//text)
    } catch * {
        ("NA")
    }
};

declare function local:check_name($doc){
    let $path := replace(document-uri($doc), 'header', 'sources')
    let $doc_source := doc($path)
    let $res := local:check_for_valid_name($doc_source)
    return
        $res
};

let $col := collection('/db/data/kolimo/header')
for $doc in $col
return
    let $srcpath := document-uri($doc)
    let $dstpath := replace($srcpath, '/db/data/kolimo/header', '/tmp/merging')


let $docs := collection("/db/data/kolimo/header/dta")
let $tab_data := for $doc in $docs
                    return
                        string-join(local:get_tabular_data_dta($doc), codepoints-to-string(9))
return
    xmldb:store("/db/tmp/", "dta_table", string-join($tab_data, codepoints-to-string(10) ))


let $docs := collection("/db/data/kolimo/header/gb")
let $tab_data := for $doc in $docs
                    return
                        string-join(local:get_tabular_data_gb($doc), codepoints-to-string(9))
return
    xmldb:store("/db/tmp/", "gb_table.txt", string-join($tab_data, codepoints-to-string(10) ))


let $docs := collection("/db/data/kolimo/header/tg")
let $tab_data := for $doc in $docs
                    return
                        string-join(local:get_tabular_data_tg($doc), codepoints-to-string(9))
return
    xmldb:store("/db/tmp/", "tg_table.txt", string-join($tab_data, codepoints-to-string(10) ))

let $doc := doc("/db/data/kolimo/header/dta/abel_leibmedicus_1699.TEI-P5.xml")
return
    local:get_author($doc)
:)
