xquery version "3.1";
declare namespace tei="http://www.tei-c.org/ns/1.0";

let $file := file:read( "/home/mgoebel/publication_and_gender/data.tsv" )
let $rows := tokenize( $file, "&#xa;" )

let $erros :=
    for $row in $rows
    let $colums := tokenize( $row, "\t" )
    let $id := replace($colums[1], "\s", "")
    let $gnd := $colums[2]
    let $k-date :=<note xmlns="http://www.tei-c.org/ns/1.0" type="kolimo-date">{$colums[3]}</note>
    let $gender := <note xmlns="http://www.tei-c.org/ns/1.0" type="author-gender">{$colums[4]}</note>
    
    let $teiHeader := collection("/db/data/kolimo/header")//tei:idno[. = $id]/ancestor::tei:teiHeader
    
    return
        try { update insert ($k-date, $gender) into $teiHeader//tei:notesStmt }
        catch * {( $id )} 

return
    xmldb:store("/db/tmp", "error-report.xml", <root> {$erros ! <item>{.}</item>} </root>)