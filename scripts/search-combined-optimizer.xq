xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";
import module namespace ksearch="http://kolimo.uni-goettingen.de/ns/search" at "../modules/search.xqm";

let $term := 'surname:Al'
let $r1 := if ( contains($term, 'surname:') ) then
    let $term := substring-after($term, 'surname:') return
    for $item in collection('/db/data/kolimo/header')//tei:author[contains(., $term)]
    where contains($item//tei:surname, $term)
    return
        ($item/following::tei:idno[@type="kolimo"])[1]
else
    for $item in collection('/db/data/kolimo/header')//tei:author[contains(., $term)]
    return
        ($item/following::tei:idno[@type="kolimo"])[1]

let $term := 'Peter'
let $r2 := if ( contains($term, 'surname:') ) then
    let $term := substring-after($term, 'surname:') return
    for $item in collection('/db/data/kolimo/header')//tei:author[contains(., $term)]
    where contains($item//tei:surname, $term)
    return
        ($item/following::tei:idno[@type="kolimo"])[1]
else
    for $item in collection('/db/data/kolimo/header')//tei:author[contains(., $term)]
    return
        ($item/following::tei:idno[@type="kolimo"])[1]

let $union := ($r1, $r2 )
let $unionString := ($union ! string(.))[index-of($union, .)[2]]

let $results := distinct-values($union[string(.) = $unionString])

return
    (
        count($r1), 
        count($r2),
        count( $results )
    )