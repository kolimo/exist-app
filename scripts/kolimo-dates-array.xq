xquery version "3.1";
(:~
 : prepares an array of distinct kolimo-dates and the number of occurences.
 : used to generate a histogram on the dashboard (index.html)
 :)

declare namespace tei="http://www.tei-c.org/ns/1.0";

let $dates := collection("/db/data/kolimo/header")//tei:note[@type="kolimo-date"]/string(.)

let $list := for $i in distinct-values( $dates )
let $count := count( $dates[. = $i] )
where matches($i, "^\d\d\d\d$")
order by $i
return
    "["||$i||","||$count||"]"

return
    string-join($list, ",")
