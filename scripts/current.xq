xquery version "3.1";
(:~
 : @author Maxi Wess
 :)

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare default element namespace "http://www.tei-c.org/ns/1.0";


declare function local:find_entry($col, $book_id as xs:string){
    for $doc in $col
        let $idno := $doc/tei:teiHeader/fileDesc/publicationStmt/idno[@type="kolimo"]
        return
            if (compare($book_id, $idno) eq 0) then (
                $doc)
            else ()
};

declare function local:db_pairs(){
    let $collection_dta := collection('/db/data/kolimo/header/dta')
    let $collection_gb := collection('/db/data/kolimo/header/gb')
    let $collection_tg := collection('/db/data/kolimo/header/tg')
    let $collections := ($collection_dta, $collection_gb, $collection_tg)
    (: let $collections := collection("/db/test_data") :)
    return
        $collections
        (:($collections//tei:teiHeader/fileDesc/publicationStmt/idno[@type="kolimo"]/text(), $collections//tei:teiHeader/fileDesc/notesStmt) :)
        (: ($collections//tei:teiHeader/fileDesc/publicationStmt/idno[@type="kolimo"]/text(), $collections//tei:teiHeader/profileDesc/textClass/keywords[@scheme="http://kolimo.uni-goettingen.de/metadata#literature-non-literature"]/list/item) :)
};


declare function local:load_id_date_pairs(){
    let $source := util:binary-to-string(util:binary-doc('/db/tmp/kolimo_gender_dofp.tsv'))
    return
        for $line in tokenize($source, '\n')
        let $tokens := tokenize($line, '\t')
(:  :        let $book_id := tokenize($line, ';')[1]
        let $gnd := tokenize($line, )
        let $year := tokenize($line, ';')[3] :)
        return
            if ((compare($tokens[3], 'nan') eq 0) or (compare($tokens[4], 'nan') eq 0)) then (
                ())
            else ($tokens[1])
};


declare function local:find_unfinished_nodes(){
    let $ids := local:load_id_date_pairs()
    let $col := local:db_pairs()
    let $allnodes := $col//tei:teiHeader/fileDesc/notesStmt
        return
            for $node in $allnodes
                return
                    if ((compare($node/note[@type='kolimo-date'][1], 'nan') eq 0) or (compare($node/note[@type='author-gender'][1], 'nan') eq 0))then ($node/note[@type="SourcePath"])
                    else (())
};

declare function local:check_node_completion($node){
        return
            if ((compare($node/note[@type='kolimo-date'][1], 'nan') eq 0) or
                (compare($node/note[@type='author-gender'][1], 'nan') eq 0) or
                (compare($node/note[@type='kolimo-date'][1]/text(), '') eq 0) or
                (compare($node/note[@type='author-gender'][1]/text(), '') eq 0)
            )
            then (True cast as xs:boolean)
            else (False cast as xs:boolean)
};

declare function local:unfinished_nodes_kolimo_id(){
    let $col := local:db_pairs()
    return
        for $doc in $col
            let $note := $doc//tei:teiHeader/fileDesc/notesStmt/note
            return
                if (local:check_node_completion($note)) then ($doc//tei:teiHeader/fileDesc/publicationStmt/idno[@type="kolimo"]/text())
                else ()
};

declare function local:get_gnd($doc){
    if ( boolean($doc/) )
}

let $kids := local:unfinished_nodes_kolimo_id()
return
    xmldb:store("/db/tmp", "unfinished_nodes_kids.txt", string-join($kids, '!!!???!!!'))
