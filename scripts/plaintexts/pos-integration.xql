xquery version "3.1";

import module namespace functx="http://www.functx.com" at "/db/system/repo/functx-1.0/functx/functx.xql";
import module namespace console="http://exist-db.org/xquery/console";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace stts="http://kolimo.uni-goettingen.de/STTS";

declare option output:method "text";
declare option output:media-type "text/plain";

let $login := xmldb:login("/db", "admin", "")

let $path:= "/home/mgoebel/POS/tagging/tmp/"
let $dir := file:directory-list($path, "*.txt")
let $dir := for $f in $dir//file:file
            let $pNum := number( substring-before(substring-after($f/@name, "-p"), ".txt") )
            order by $pNum
            return
                string( $f/@name )

let $results := for $file at $position in $dir[position() != (488, 1934)]
    let $test := console:log( ($position) )
    let $test := console:log( ($file) )

    let $docpathPart1 := replace(substring-before(substring-after($file, "s_tg_"), "__"), "_", "-")
    let $docpathPart2 := substring-before(substring-after($file, "__"), "-p")
    let $resource-name := $docpathPart1 || "__" || $docpathPart2 || ".xml"
    let $docPath := "/db/data/kolimo/sources/tg/" || $resource-name
    let $pNum := number( substring-before(substring-after($file, "-p"), ".txt") )

    let $URIpath := "/db/data/kolimo/POS/tg/"|| $resource-name
    let $doc := doc( $URIpath )

    let $tei := doc($docPath)
    let $p := string-join( ($tei//tei:body//tei:p)[$pNum]//text(), " " )
    let $wSeq := functx:get-matches-and-non-matches( $p, "\W"  )[not(matches(., "^\s+$"))]

    let $tsv := file:read($path || $file)
    let $rows := tokenize( $tsv, "\n" )[. != "treetagger"]
    let $rows :=
        for $row in $rows[ . != "" ]
            return
                switch (tokenize($row, "\t")[1])
                case '",' return    ( '"&#x9;$(', ',&#x9;$.' )
                case ',"' return    ( ',&#x9;$.', '"&#x9;$(' )
                case '!",' return   ( '!&#x9;$.', '"&#x9;$(', ',&#x9;$,')
                case '!"' return    ( '!&#x9;$.', '"&#x9;$(' )
                case '?"' return    ( '?&#x9;$.', '"&#x9;$(' )
                case '?!' return    ( '?&#x9;$.', '!&#x9;$.' )
                case '."' return    ( '.&#x9;$.', '"&#x9;$(' )
                case '–"' return    ( '–&#x9;$(', '"&#x9;$(' )
                default return $row
    let $STTS := <p xmlns="http://kolimo.uni-goettingen.de/STTS" n="{$pNum}">
                        {
                            for $row at $pos in $rows[position() lt last()]
                            let $POS := tokenize($row, "\t")[2]
                            let $POS := switch ($POS)
                                case "$," return "INT1"
                                case "$." return "INT2"
                                case "$(" return "INT3"
                                default return $POS
                            return
                                if(string($POS) = "") then console:log( ($file, " ", $row, " ", $pos ) ) else
                                element {($POS)} {}
                        }
                    </p>
    
(: DEBUG!!!!
 : DEBUG!!!!
 : DEBUG!!!!
 : 
 :    return:)
(:        <root n="{$position}">{:)
(:            :)
(:        for $item at $pos in $wSeq:)
(:        return:)
(:            <item n="{format-number($pos, "000")}">{($item/text(), " " ,$rows[$pos])}</item>:)
(:        }:)
(:        </root>:)
    return
        if(doc-available($URIpath))
        then
            if($doc//stts:p/@n != $pNum)
            then update insert $STTS into $doc/stts:body
            else ()
        else
            let $POSdoc := 
                <body xmlns="http://kolimo.uni-goettingen.de/STTS">
                    { $STTS }
                </body>
            return
                xmldb:store("/db/data/kolimo/POS/tg/", $resource-name, $POSdoc)

return
    (
        count( $dir ),
        count( $results )
    )