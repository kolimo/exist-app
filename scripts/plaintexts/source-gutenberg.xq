xquery version "3.1";
import module namespace console="http://exist-db.org/xquery/console";

declare variable $pathFS := '/home/mgoebel/plaintext/gb-zip';
declare variable $pathDB := '/db/data/kolimo/sources/gb';

(:let $start := 18 * 1000:)
let $path := $pathFS
let $cleanup := file:delete($path)
let $store := file:mkdir($path)
let $filelist := file:list($path)//file:file/string(@name)

(: get list of files we have to create :)
let $names := ( collection( $pathDB )//body/substring-after(replace(replace(base-uri(), '[^\w]', '_' ), '_xml$', '.zip'), '_') )
let $ToBeProcessed := for $n in $names where not($n = $filelist) return $n
let $console := console:log( count( $ToBeProcessed ) )
let $console := console:log( ($ToBeProcessed ! <item>{.}</item>) )
(: set interval for max number of files in a single zip archive :)
let $interval := 3000

let $text :=    
                for $body at $pos in (collection($pathDB)//body[substring-after(replace(replace(base-uri(), '[^\w]', '_' ), '_xml$', '.zip'), '_') = $ToBeProcessed])
(:                [(position() gt $start) and (position() lt ($start + 2001))]:)
(:                for $body in doc('/db/data/kolimo/textgrid-tei/1001-Altenberg__Peter.xml')//tei:body:)
                let $zipFileName := substring-after(replace(replace( $body/base-uri(), '[^\w]', '_' ), '_xml$', '.zip'), '_')
(:                where not($zipFileName = $filelist):)
                let $countP := count( $body//p )
                let $iterations := xs:integer(ceiling($countP div $interval))
                return
                    for $iteration in 1 to $iterations

                    let $console := console:log( ($zipFileName || ': ' || $iteration || ' of ' || $iterations ) )

                    let $minPPos := ($iteration - 1) * $interval
                    let $maxPPos := $iteration * $interval + 1
                    let $entries :=
                        for $item at $posi in ($body//p)[(position() gt $minPPos) and (position() lt $maxPPos)]
(:                        let $console := if($posi mod 100 = 0) then console:log( ($posi) ) else ():)
                        let $pos := $posi + $minPPos
                        let $counter := $pos
                        let $filename := substring-after(replace(replace( $item/base-uri(), '[^\w]', '_' ), '_xml$', '-p'||$counter||'.txt'), '_')
                        let $filename := $path||$filename
                        let $string := replace( string-join($item//text(), ' '), '\s+', ' ')
    (:                    let $base64 := xs:base64Binary(util:base64-encode( $string )):)
                        let $entry := <entry name="{$filename}" type="text">{$string}</entry>
                        return $entry
                    let $zipfolder := xs:base64Binary( compression:zip($entries, false()) )
                    let $zipFileName := if($iteration gt 1) then substring-before($zipFileName, '.zip') || '-part' || $iteration || '.zip' else $zipFileName
                    return
                        (file:serialize-binary( $zipfolder , $path || $zipFileName))

return
  count( $text )