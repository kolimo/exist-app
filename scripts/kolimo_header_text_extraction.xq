xquery version "3.1";
(:~
 : @author Maxi Wess
 :)

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace default="http://www.tei-c.org/ns/1.0";

declare function local:get_text_gb($doc){
    string-join($doc//body//text(), codepoints-to-string(10))
};

declare function local:get_text_tg($doc){
    string-join($doc//tei:text//text(), codepoints-to-string(10))
};


for $doc in collection('/db/data/kolimo/header/tg')
return
    let $srcpath := document-uri($doc)
    let $fname := tokenize($srcpath, '/')[last()]
    let $dstpath := replace($srcpath, '/db/data/kolimo/header', '/tmp/merging')
    let $sourcespath := replace($srcpath, '/db/data/kolimo/header', '/db/data/kolimo/sources')
    let $cleartext_document := doc($sourcespath)
    let $dstdir := replace($dstpath, $fname, '')
    let $cleartexts := local:get_text_tg($cleartext_document)
    let $header := $doc//tei:teiHeader[1]
    let $newheader := <TEI>{$header, <text>{$cleartexts}</text>}</TEI>
    return
        file:serialize($newheader, $dstpath, '', false())
