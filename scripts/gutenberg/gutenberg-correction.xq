xquery version "3.1";
(:~
 : @deprecated collection not available
 :)

let $col := collection('/db/data/kolimo/gutenberg/')
return
    (for $i in $col//meta[@name=" title"]
    return
        update replace $i/@name with attribute name { 'title' }
    ,
    for $i in $col//meta[@name=""]
    return
        update delete $i
    ,
    for $i in $col//meta[@name="2corrector"]
    return
        update replace $i/@name with attribute name { 'secondcorrector' }
    ,
    for $i in $col//meta[@name="2corrected"]
    return
        update replace $i/@name with attribute name { 'secondcorrected' }
    ,
    for $i in $col//meta[@name="auhthor"]
    return
        update replace $i/@name with attribute name { 'author' }
    ,
    for $i in $col//meta[@name="Illustrator"]
    return
        update replace $i/@name with attribute name { 'illustrator' }
    ,
    for $i in $col//meta[@name="illustator"]
    return
        update replace $i/@name with attribute name { 'illustrator' }
    ,
    for $i in $col//meta[@name="ISBN"]
    return
        update replace $i/@name with attribute name { 'isbn' }
    ,
    for $i in $col//meta[@name="keyword"]
    return
        update replace $i/@name with attribute name { 'keywords' }
    ,
    for $i in $col//meta[@name="rtranslator"]
    return
        update replace $i/@name with attribute name { 'translator' }
    ,
    for $i in $col//meta[@name="scorrector"]
    return
        update replace $i/@name with attribute name { 'corrector' }
    ,
    for $i in $col//meta[@name="src"]
    return
        update replace $i/@name with attribute name { 'source' }
    ,
    for $i in $col//meta[@name="tpye"]
    return
        update replace $i/@name with attribute name { 'type' }
    ,
    for $i in $col//meta[@name="tranlator"]
    return
        update replace $i/@name with attribute name { 'translator' }
    ,
    for $i in $col//meta[@name="author"][ends-with(@content, '&lt;')]
    let $newcontent := substring-before($i/@content, '&lt;')
    return
        update replace $i/@content with attribute content { $newcontent }
    )
