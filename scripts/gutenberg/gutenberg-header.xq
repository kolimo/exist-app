xquery version "3.1";
(:~
 : @deprecated collection not available
 :)

import module namespace console="http://exist-db.org/xquery/console";
let $collection-uris := '/db/data/kolimo/gutenberg'
let $gbHeaderCol := '/db/data/kolimo/header/gb/'
return

for $HTML at $pos in (collection($collection-uris)//html)
let $console := if($pos mod 500 = 0) then
    console:log( ($pos) ) else ()
let $newCollections := tokenize( substring-after($HTML/base-uri(), '/kolimo/gutenberg/'), '/' )[position() lt last()]
let $createCollections := for $col at $pos in $newCollections return xmldb:create-collection(string-join(($gbHeaderCol, $newCollections[position() lt $pos]), '/'), $col)
let $filename := tokenize( $HTML/base-uri(), '/' )[last()]
let $author := $HTML//meta[@name="author"]/string(@content)
let $author :=  if( count(tokenize($author, ',')) = 2 )
                then (  <forename xmlns="http://www.tei-c.org/ns/1.0">{replace(substring-after($author, ','), '^\s|\s$', '')}</forename>,
                        <surname xmlns="http://www.tei-c.org/ns/1.0">{replace(substring-before($author, ','), '^\s|\s$', '')}</surname>)
                else if( count(tokenize($author, ' ')) = 2 )
                then  (  <forename xmlns="http://www.tei-c.org/ns/1.0">{tokenize($author, ' ')[1]}</forename>,
                        <surname xmlns="http://www.tei-c.org/ns/1.0">{tokenize($author, ' ')[2]}</surname>)
                else
                    <forename xmlns="http://www.tei-c.org/ns/1.0">{$author}</forename>
let $title := $HTML//meta[@name="title"]/string(@content)
let $subtitle := $HTML//body/h4[@class="subtitle"]/text()
let $genre := $HTML/head//meta[@name="type"]/string(@content)
let $pubStmt := <publicationStmt xmlns="http://www.tei-c.org/ns/1.0">
                {if(exists($HTML//meta[@name="publisher"])) then <publisher><name>{$HTML//meta[@name="publisher"]/string(@content)}</name></publisher> else ()}
                {if(exists($HTML//meta[@name="address"])) then <pubPlace>{$HTML//meta[@name="address"]/string(@content)}</pubPlace> else ()}
                {if(exists($HTML//meta[@name="year"])) then <date type="publication">{$HTML//meta[@name="year"]/string(@content)}</date> else ()}
                </publicationStmt>
let $seriesStmt := if(exists($HTML//meta[@name="series"]) or exists($HTML//meta[@name="volume"])) then
                            <seriesStmt xmlns="http://www.tei-c.org/ns/1.0">
                                <title level="s" type="main">{$HTML//meta[@name="series"]/string(@content)}</title>
                                <biblScope unit="volume">{$HTML//meta[@name="volume"]/string(@content)}</biblScope>
                            </seriesStmt> else ()
let $creation:= if(exists($HTML//meta[@name="firstpub"]))
                then <date xmlns="http://www.tei-c.org/ns/1.0" type="firstPublication">{$HTML//meta[@name="firstpub"]/string(@content)}</date>
                else()
let $doc := document {
(:        processing-instruction xml {'version="1.0" encoding="UTF-8"'},:)
        processing-instruction xml-model {'href="http://media.dwds.de/dta/media/schema/basisformat.rng"'},
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title type="main">{$title}</title>
                {if($subtitle != '') then <title type="sub">{$subtitle}</title> else ()}
                <author>
                    <persName>
                        {$author}
                    </persName>
                </author>
                <respStmt xml:id="textsource-1" corresp="#availability-textsource-1">
                    <orgName>Projekt Gutenberg DE - Hille &amp; Partner</orgName>
                    <resp>
                        <note type="remarkResponsibility">Bereitstellung der Texttranskription und
                            Basismetadaten</note>
                    </resp>
                </respStmt>
                <respStmt>
                    <orgName ref="http://www.textgrid.de">TextGrid</orgName>
                    <resp>
                        <note type="remarkResponsibility">Langfristige Bereitstellung der
                            Dokumente</note>
                        <ref target="http://textgridrep.de"/>
                    </resp>
                </respStmt>
            </titleStmt>
            <editionStmt>
                <edition>Vollständige digitalisierte Ausgabe.</edition>
            </editionStmt>
            <extent>
                <measure type="tokens"/>
                <measure type="types"/>
                <measure type="characters"/>
            </extent>
            <publicationStmt>
                <publisher xml:id="kolimo">
                    <!-- invalid according to DTA-Bf-Schema, must be "DTACorpusPublisher" -->
                    <email>jb.herrmann@phil.uni-goettingen.de</email>
                    <orgName role="project">Kolimo - Korpus der Literarischen Moderne</orgName>
                    <orgName role="hostingInstitution" xml:lang="de">Seminar für Deutsche
                        Philologie, Georg-August-Universität Göttingen</orgName>
                    <address>
                        <addrLine>Käthe-Hamburger-Weg 3, 37073 Göttingen</addrLine>
                        <country>Germany</country>
                    </address>
                </publisher>
                <pubPlace>Göttingen</pubPlace>
                <date type="publication">2016-06</date>
                <availability xml:id="availability-textsource-1" corresp="#textsource-1">
                    <licence target="http://gutenberg.spiegel.de/information">
                        <p>nicht-kommerzielle Nutzung frei</p>
                    </licence>
                </availability>
            <idno type="kolimo">gb{$pos}</idno>
            </publicationStmt>
            <sourceDesc>
                <bibl type="MS"/>
                <biblFull>
                    <titleStmt>
                        <title level="m" type="main">{$title}</title>
                        {if($subtitle != '') then <title level="m" type="sub">{$subtitle}</title> else ()}
                        <author>
                            <persName>
                                {$author}
                            </persName>
                        </author>
                    </titleStmt>
                    <publicationStmt>
                        <publisher>Hille &amp; Partner</publisher>
                    </publicationStmt>
                    <seriesStmt>
                        <title type="main">Projekt Gutenberg DE</title>
                        <idno type="URLXML">dvd13://{substring-after($HTML/base-uri(), 'gutenberg/')}</idno>
                        {if(exists($HTML//meta[@name="projectid"])) then (<idno type="GutenbergID">{$HTML//meta[@name="projectid"]/string(@content)}</idno>,<!-- GutenbergID is not a valid value -->) else ()}
                    </seriesStmt>
                    <sourceDesc>
                        <biblFull>
                            <titleStmt>
                                <title level="m" type="main">{$title}</title>
                                {if($subtitle != '') then <title level="m" type="sub">{$subtitle}</title> else ()}
                                <author>
                                    <persName>
                                        {$author}
                                    </persName>
                                </author>
                            </titleStmt>
                            {$pubStmt}
                            {$seriesStmt}
                        </biblFull>
                    </sourceDesc>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <encodingDesc>
            <editorialDecl>
                <p>
                    Bogensignaturen: keine Angabe; Druckfehler: ignoriert; fremdsprachliches
                    Material: keine Angabe; Geminations-/Abkürzungsstriche: keine Angabe;
                    Hervorhebungen (Antiqua, Sperrschrift, Kursive etc.): keine Angabe; i/j in
                    Fraktur: keine Angabe; I/J in Fraktur: keine Angabe; Kolumnentitel: keine
                    Angabe; Kustoden: keine Angabe; langes s (ſ): als s transkribiert;
                    Normalisierungen: stillschweigend; rundes r (&amp;#xa75b;): als r/et
                    transkribiert; Seitenumbrüche markiert: ja; Silbentrennung: keine Angabe; u/v
                    bzw. U/V: keine Angabe; Vokale mit übergest. e: als ä/ö/ü transkribiert;
                    Vollständigkeit: vollständig erfasst; Zeichensetzung: keine Angabe;
                    Zeilenumbrüche markiert: nein; </p>
            </editorialDecl>
        </encodingDesc>
        <profileDesc>
            <creation>{$creation}</creation>
            <textClass>
                <keywords scheme="http://gutenberg.spiegel.de/genre/">
                    <list>
                        <item>
                            {$genre}
                        </item>
                    </list>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
</TEI>
}

return

    xmldb:store(string-join(($gbHeaderCol, $newCollections), '/'), $filename, $doc)
