xquery version "3.1";
(:~
 : @deprecated collection not available
 :)

let $authors := for $meta in collection('/db/data/kolimo/gutenberg/')//meta[@name = "author"]
                return
                    try { if($meta/@name = "author") then $meta/string-join(@content) else ()}
                catch * { $meta/base-uri() || $err:description }
let $distinctAuthors := distinct-values( $authors  )

return
    for $i in $distinctAuthors
    order by lower-case($i)
    return $i || '
'
