xquery version "3.1";
(:~
 : @deprecated collection not available
 :)

for $i in distinct-values( collection('/db/data/kolimo/gutenberg/')//meta[string(@name) = 'type']/string(@content) )
order by lower-case($i)
return $i || '
'
