xquery version "3.1";
(:~
 : @author Maxi Wess
 :)

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare default element namespace "http://www.tei-c.org/ns/1.0";
import module namespace functx="http://www.functx.com" at "/db/system/repo/functx-1.0/functx/functx.xql";


declare function local:convert_date($date){
    let $matches := functx:get-matches($date, "\d{4}")
    let $seq := for $v in $matches
                order by -1*string-length($v)
                    return $v
    return
        if (functx:all-whitespace($seq[1])) then 0 else
        xs:integer($seq[1])
};
    (:)
    let $matches := if (empty($date)) then ("") else (functx:get-matches($date, '\d\d\d\d')[2])
    return
        ($date, $matches) :)
         (: if (functx:all-whitespace($matches)) then 0 else (xs:integer($matches)) :)
    (:
    (:return
        if (contains($date, "/"))
            then (xs:integer(functx:substring-after-if-contains(text(), "/")))
            else if (contains($date, " ")) then (
                    xs:integer(tokenize($date, " ")[2]))
                    else
                        xs:integer($date)
      :) :)

declare function local:get-earliest-date($creation){
    let $date_string := if (functx:all-whitespace($creation/date[@type="firstPublication"]/text())) then
            (if(exists($creation/date[@type="firstPublication"]/date[@notBefore])) then ($creation/date[@type="firstPublication"]/date/@notBefore/string())
                else ( if (exists($creation/date[@type="OriginalSourcePublication"])) then ($creation/date[@type="OriginalSourcePublication"]/text()) else ()
                    )
            )
            else ($creation/date[@type="firstPublication"]/text())
    return
        local:convert_date($date_string)

};

declare function local:get-pairs(){
    let $collection_dta := collection('/db/data/kolimo/header/dta')
    let $collection_gb := collection('/db/data/kolimo/header/gb')
    let $collection_tg := collection('/db/data/kolimo/header/tg')
    (: let $collections := ($collection_dta, $collection_gb, $collection_tg) :)
    let $collections : = collection('/db/test_data/kolimo/header')
    return
        for $doc in $collections
            return
                (functx:substring-after-last(base-uri($doc), "/"), local:get-earliest-date($doc//tei:teiHeader/profileDesc/creation))

};



let $collection_dta := collection('/db/data/kolimo/header/dta')
let $collection_gb := collection('/db/data/kolimo/header/gb')
let $collection_tg := collection('/db/data/kolimo/header/tg')
let $collections := ($collection_dta, $collection_gb, $collection_tg)
(:  :let $collections := collection('/db/test_data/kolimo/header') :)
(: let $collections := collection('/db/test_data/kolimo/sources') :)


let $doc := "abel_leibmedicus_1699.TEI-P5.xml"
let $header_prefix := '/db/data/kolimo/header/'
let $source_prefix := '/db/data/kolimo/sources/'
let $list := for $doc in $collections
                return
                    if ((local:get-earliest-date($doc//tei:teiHeader/profileDesc/creation) gt 1880) and (local:get-earliest-date($doc//tei:teiHeader/profileDesc/creation) lt 1930) and ($doc//tei:teiHeader/profileDesc/textClass/keywords[@scheme="http://kolimo.uni-goettingen.de/metadata#literature-non-literature"]/list/item/text() = 'literature')) then
                    (replace(base-uri($doc), $header_prefix, "")) else ()
return
    xmldb:store("/db/tmp", "filelist.txt", string-join($list, ' '))
        (: TODO: filter out pairs earlier than 1800 and only use pairs that are literature. then cleartext:)
        (:)
        if (functx:all-whitespace($doc//tei:teiHeader/profileDesc/creation/date[@type="firstPublication"]/text())) then
            (if(exists($doc//tei:teiHeader/profileDesc/creation/date[@type="firstPublication"]/date[@notBefore])) then ($doc//tei:teiHeader/profileDesc/creation/date[@type="firstPublication"]/date/@notBefore/string())
                else ( if (exists($doc//tei:teiHeader/profileDesc/creation/date[@type="OriginalSourcePublication"])) then ($doc//tei:teiHeader/profileDesc/creation/date[@type="OriginalSourcePublication"]/text()) else ()
                    )
            )
            else ($doc//tei:teiHeader/profileDesc/creation/date[@type="firstPublication"]/text())
        :)
    (: string-join(distinct-values($collections//tei:teiHeader/profileDesc/creation/date[@type="firstPublication"]/text()), " ")
    count($collections//tei:teiHeader/profileDesc/creation/date[@type="firstPublication" and local:convert_date(text()) gt 1800])
    count($collections//tei:teiHeader/profileDesc/creation/date[@type="firstPublication" and functx:all-whitespace(text())]) :)
(:
return
    xmldb:store("tmp/", "test", string-join(doc($source)//tei:text//text()))
$collections//tei:teiHeader/profileDesc/creation/date[@type="firstPublication" and xs:integer($collections//tei:teiHeader/profileDesc/creation/date/text()) gt 1800]






:)
