xquery version "3.1";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace tei="http://www.tei-c.org/ns/1.0";

declare option output:method "html5";
declare option output:media-type "text/html";

<table>
    <th>Datei</th>
{
    for $header in collection("/db/data/kolimo/header")//tei:teiHeader
    let $pubDate := $header//tei:sourceDesc//tei:publicationStmt//tei:date
    let $date := (
        $pubDate[@notBefore < "1700"],
        $pubDate[@when < "1700"],
        $pubDate[not(@when|@notBefore)][. < "1700"],
        $header//tei:note[@type="kolimo-date"][. < "1700"]
    )
    let $useDate := $date[1]
    order by $useDate ascending
    return
        <tr>
            <td>{util:document-name($useDate)}</td>
            {
                for $d in $date
                return
                    <td>{($d/@when/string(), $d/text())}</td>
            }
        </tr>
}
</table>
