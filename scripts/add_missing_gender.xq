xquery version "3.1";
(:~
 : @author Maxi Wess
 :)

declare default element namespace "http://www.tei-c.org/ns/1.0";

declare function local:missing_info_kid(){
let $source := util:binary-to-string(util:binary-doc('/db/tmp/kolimo_missing-gender_KorrHH.csv'))
return
    let $lines := tokenize($source, codepoints-to-string(10))
    for $line in $lines
        return
            let $lsplit := tokenize($line, ',')
            let $kid := $lsplit[1]
            let $gender := $lsplit[5]
            return
                $kid
};

declare function local:missing_info_gender(){
let $source := util:binary-to-string(util:binary-doc('/db/tmp/kolimo_missing-gender_KorrHH.csv'))
return
    let $lines := tokenize($source, codepoints-to-string(10))
    for $line in $lines
        return
            let $lsplit := tokenize($line, ',')
            let $kid := $lsplit[1]
            let $gender := $lsplit[5]
            return
                $gender
};


declare function local:check_notes_node($doc){
    if (not(exists($doc//teiHeader/fileDesc/notesStmt))) then () else ()
};

declare function local:get_kid($doc){
    $doc//teiHeader/fileDesc/publicationStmt/idno[@type="kolimo"]/text()
};

declare function local:get_gender($doc){
    $doc//teiHeader/fileDesc/notesStmt/note[@type="author-gender"][1]/text()
};

declare function local:get_gender_from_list($list, $kid){
    let $kids := for $pair in $list
                    return
                        $pair[1]
    let $pos := index-of($kids, $kid)
    return
        $list[$pos]
};

declare function local:gender_in_list($list, $kid){
    let $kids := for $pair in $list
                    return
                        $pair[1]
    return
        if ($kids = $kid) then (true()) else (false())
};

declare function local:check_notes_node($doc){
    let $sourcepath := replace(document-uri($doc), '/db/data/header/gb/' ,'')
    let $empty_notes := <notesStmt><!-- TextGrid offers some metadata in a path-like value of TEI/@n. I comes from the original source zeno.org.
                                 From Gutenberg we fill this value with the physical file path from the DVD13.-->
                            <note type="SourcePath">{$sourcepath}</note>
                            <note type="kolimo-date">nan</note>
                            <note type="author-gender">nan</note>
                        </notesStmt>
    return
        if (not(exists($doc//teiHeader/fileDesc/notesStmt))) then
            (update insert $empty_notes into $doc//teiHeader/fileDesc)
            else ()
};

let $missing_info_kid := local:missing_info_kid()
let $missing_info_gender := local:missing_info_gender()
return
    for $doc in collection('/db/data/kolimo/header/')
        let $notecheck := local:check_notes_node($doc)
        let $kid := local:get_kid($doc)
        return
            if (local:gender_in_list($missing_info_kid, $kid)) then (
                let $pos := index-of($missing_info_kid, $kid)
                let $gender := $missing_info_gender[$pos]
                    return
                        update replace $doc//teiHeader/fileDesc/notesStmt/note[@type='author-gender']/text() with $gender
                ) else ()
