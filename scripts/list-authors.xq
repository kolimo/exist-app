xquery version "3.1";
(:  :import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule"; :)
import module namespace console="http://exist-db.org/xquery/console";
declare namespace tei="http://www.tei-c.org/ns/1.0";


let $collection-tg := '/db/data/kolimo/header/tg'
let $collection-gb := '/db/data/kolimo/header/gb'
let $collection-dta := '/db/data/kolimo/header/dta'

let $console := console:log( "START" )

let $collection-uris := '/db/data/kolimo/header/dta'
let $collection := collection($collection-uris)

    let $author :=  switch ($source)
                        case 'gutenberg'    return $DOC//*:meta[@name="author"]/string(@content)
                        case 'textgrid'     return $DOC//tei:biblFull[1]/tei:titleStmt[1]/tei:author[1]/text()
                        case 'dta'          return $DOC//tei:teiHeader[1]/tei:fileDesc[1]/tei:titleStmt[1]/tei:author[1] 
                        default return ()

let $authors := distinct-values($collection//tei:fileDesc/tei:titleStmt/tei:author/tei:persName/string-join(.//text()))

let $console := console:log( ($authors ) )
return $console