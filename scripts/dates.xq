xquery version "3.1";
declare namespace tei="http://www.tei-c.org/ns/1.0";

(: GUTENBERG :)
let $collection-uris := '/db/data/kolimo/gutenberg'
let $collection := collection($collection-uris)
let $datesGB := $collection//meta[@name="firstpub"]/number(@content)

(: TEXTGRID :)
let $collection-uris := '/db/data/kolimo/textgrid-tei/'
let $collection := collection($collection-uris)
let $datesTG :=
    for $header in $collection//tei:teiHeader
    let $date := ($header//tei:date[not(. = '2016-06')])[1]
    return
        if($date/@when) then number($date/@when) else
        if($date/text()) then number($date/text()) else
        if($date/@notBefore and $date/@notAfter) then
            let $num0 := $date/number(@notBefore)
            let $num1 := $date/number(@notAfter)
            return
                if( ($num1 - $num0)) then $date/number(@notAfter)
                else ( $num1 - (($num1 - $num0) div 3) )
        else ()

return
    string-join(for $d in distinct-values((($datesGB, $datesTG))[string(.) != "NaN"])[. != 1926]
    let $d := if(string-length($d) gt 4) then number(substring(replace($d, '\.', ''), 0, 5)) else $d
    order by $d
    return
        ('['||$d||','||count( (($datesGB, $datesTG))[. = $d] )||']' ), ',')
