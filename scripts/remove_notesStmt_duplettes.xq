xquery version "3.1";
(:~
 : @author Maxi Wess
 :)

declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function local:getNotesStmt($doc){
    $doc//tei:teiHeader/tei:fileDesc/tei:notesStmt
};

let $col := collection('/db/data/kolimo/header')
for $doc in $col
(: let $doc := doc('/db/data/kolimo/header/gb/about/badenbad/badenbad.xml'):)
return
    let $notesStmtOld := local:getNotesStmt($doc)
    return
        if (exists($notesStmtOld)) then (
            let $sp := $notesStmtOld/tei:note[@type="SourcePath"]/text()
            let $kd := $notesStmtOld/tei:note[@type="kolimo-date"][1]/text()
            let $ag := $notesStmtOld/tei:note[@type="author-gender"][1]/text()
            let $notesNew := <notesStmt xmlns="http://www.tei-c.org/ns/1.0">
                                <note type="SourcePath">{$sp}</note>
                                <note type="kolimo-date">{$kd}</note>
                                <note type="author-gender">{$ag}</note>
                             </notesStmt>
                return
                    update replace $notesStmtOld with $notesNew
                    (:($notesNew, $notesStmtOld):)
        ) else (document-uri($doc))
