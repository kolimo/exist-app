xquery version "3.1";
(:~
 : @author Maxi Wess
 :)

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare default element namespace "http://www.tei-c.org/ns/1.0";

declare function local:get_kid($doc){
    $doc//teiHeader/fileDesc/publicationStmt/idno[@type="kolimo"]/text()
};

(:  :let $doc := doc('/db/data/kolimo/header/dta/abel_leibmedicus_1699.TEI-P5.xml') :)
let $col := collection('/db/data/kolimo/header')
let $liste := for $doc in $col
return
    let $kid := local:get_kid($doc)
    let $title := $doc//teiHeader/fileDesc/titleStmt/title[@type="main"]/text()
    let $tpl := string-join(($kid, $title), codepoints-to-string(9))
    return
        if (exists($doc//teiHeader/fileDesc/sourceDesc/biblFull/publicationStmt/pubPlace/text())) then () else ($tpl)
return
    xmldb:store('/db/tmp/', 'missing_pub_places', string-join($liste, codepoints-to-string(10)))
