xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";

<ul>
{
    for $header in collection("/db/data/kolimo/header")//tei:teiHeader
    let $pubDate := $header//tei:sourceDesc//tei:publicationStmt//tei:date
    let $date := (
        $pubDate[@notBefore < "1700"],
        $pubDate[@when < "1700"],
        $pubDate[not(@when|@notBefore)][. < "1700"],
        $header//tei:note[@type="kolimo-date"][. < "1700"]
    )[1]
    order by $date ascending
    return
        <li>{document-uri(root($date))}: {$date}</li>
}
</ul>