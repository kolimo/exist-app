xquery version "3.1";
import module namespace console="http://exist-db.org/xquery/console";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare function local:gndSearch($author){
    let $author := if(count(tokenize($author, " ")) = 2) then tokenize($author, " ")[2] || ', ' || tokenize($author, " ")[1] else $author
    let $url1 := "https://portal.dnb.de/opac.htm?method=moveDown&amp;currentResultId=%22" || encode-for-uri($author) ||"%22+and+sw%3DSchriftsteller%26any&amp;categoryId=persons" 
    let $get1 := httpclient:get($url1, false(), ())//xhtml:html/xhtml:body/xhtml:div[5]/xhtml:div[3]/xhtml:table[1]
    return
        (
            if( $get1//xhtml:strong/text() = "Link zu diesem Datensatz" ) then ($get1//xhtml:a/text())[1]
            else if(count( $get1//xhtml:a ) lt 6) then 
                for $get2 in $get1//xhtml:a/('https://portal.dnb.de' || string(@href) )
                return (httpclient:get($get2, false(), ())//xhtml:html/xhtml:body/xhtml:div[5]/xhtml:div[3]/xhtml:table[1]//xhtml:a/text() )[1]
            else ()
        )
};

declare function local:keepDirStructure($base-uri, $targetCollection){
    let $newCollections := tokenize( substring-after($base-uri, '/kolimo/sources/gb/'), '/' )[position() lt last()]
    let $createCollections := 
        for $col at $pos in $newCollections return xmldb:create-collection(string-join(($targetCollection, $newCollections[position() lt $pos]), '/'), $col) 
    return true()
};

(: set overwrite to false, if you want to preserve existing documents :)
let $overwrite := false()

let $collection-tg := '/db/data/kolimo/sources/tg'
let $collection-gb := '/db/data/kolimo/sources/gb'
let $collection-dta := '/db/data/kolimo/sources/dta'
let $targetPath := '/db/data/kolimo/header/'

let $collectionTest:=   for $col at $pos in tokenize($targetPath, '/')
                        let $path := string-join(tokenize($targetPath, '/')[position() lt $pos], '/')
                        let $completePath := string-join((tokenize($targetPath, '/')[position() lt $pos], $col), '/')
                        return 
                            if(xmldb:collection-available($completePath)) 
                            then true() 
                            else xmldb:create-collection($path, $col)

return
for $DOC at $pos in ((collection($collection-gb)//html), (collection($collection-tg)//tei:TEI), (collection($collection-dta)//tei:TEI))
    (: every 1K document send an output to the console, to let us know that the script is running well :)
    let $console := if($pos mod 1000 = 0) then console:log( ($pos) ) else ()
    
    (: determine on what corpus we are working :)
    let $source :=  if(contains($DOC/base-uri(), '/kolimo/sources/gb/')) then 'gutenberg' else 
                    if(contains($DOC/base-uri(), '/kolimo/sources/tg')) then 'textgrid' else
                    if(contains($DOC/base-uri(), '/kolimo/sources/dta')) then 'dta' else ()
    
    let $targetCollection := $targetPath || 
                                (switch ($source)
                                    case 'gutenberg' return 
                                        let $test := if(xmldb:collection-available($targetPath || 'gb/')) then true() else xmldb:create-collection($targetPath, 'gb')
                                        return 'gb/' || string-join( tokenize(substring-after($DOC/base-uri(), '/sources/gb/'), '/' )[position() lt last()], '/') || '/'
                                    case 'textgrid' return 
                                        let $test := if(xmldb:collection-available($targetPath || 'gb/')) then true() else xmldb:create-collection($targetPath, 'tg')
                                        return 'tg/'
                                    case 'dta' return 
                                        let $test := if(xmldb:collection-available($targetPath || 'gb/')) then true() else xmldb:create-collection($targetPath, 'dta')
                                        return 'dta/'
                                    default return ())

    (: keep directory structure for gutenberg :)
    let $keepDirStructure := if($source = "gutenberg") then local:keepDirStructure($DOC/base-uri(), $targetPath || 'gb') else ()
    
    let $filename := tokenize( $DOC/base-uri(), '/' )[last()]
    return
    (: store the new header in the corresponding collections :)
    if(doc-available($targetCollection || $filename) and $overwrite = false() ) then false() else
    (: find basic metadata :)
    let $author :=  switch ($source)
                        case 'gutenberg'    return $DOC//*:meta[@name="author"]/string(@content)
                        case 'textgrid'     return $DOC//tei:biblFull[1]/tei:titleStmt[1]/tei:author[1]/text()
                        case 'dta'          return $DOC//tei:teiHeader[1]/tei:fileDesc[1]/tei:titleStmt[1]/tei:author[1] 
                        default return ()
    let $author :=  switch ($source)
                        (: nothing to do for DTA documents :)
                        case 'dta' return $author
                        default return
                            (: we expect to find an id in textgrid documents  :)
                            let $gnd := $DOC//tei:teiHeader[1]//tei:author[@key]/substring-after(@key, ':')
                            (: and so this is an empty string if we deal with a gutenberg text :)
                            let $gnd := if($gnd != "") 
                                        then "http://d-nb.info/gnd/" || $gnd
                                        else local:gndSearch($author)
                            return
                                if( count(tokenize($author, ',')) = 2 )
                                then    <author xmlns="http://www.tei-c.org/ns/1.0">
                                            <persName ref="{$gnd}">
                                                <forename>{replace(substring-after($author, ','), '^\s|\s$', '')}</forename> 
                                                <surname>{replace(substring-before($author, ','), '^\s|\s$', '')}</surname>
                                            </persName>
                                        </author>
                                else 
                                if( count(tokenize($author, ' ')) = 2 )
                                then    <author xmlns="http://www.tei-c.org/ns/1.0">
                                            <persName ref="{$gnd}">
                                                <forename>{tokenize($author, ' ')[1]}</forename>
                                                <surname>{tokenize($author, ' ')[2]}</surname>
                                            </persName>
                                        </author>
                                else
                                    <author xmlns="http://www.tei-c.org/ns/1.0">
                                        <persName ref="{$gnd}">
                                            <forename>{$author}</forename>
                                        </persName>
                                    </author>

    let $title := switch ($source)
                        case 'gutenberg'    return $DOC//*:meta[@name="title"]/string(@content)
                        case 'textgrid'     return $DOC//tei:teiHeader[1]/tei:fileDesc[1]/tei:titleStmt[1]/tei:title[1]/text()
                        case 'dta'          return $DOC//tei:teiHeader[1]/tei:fileDesc[1]/tei:titleStmt[1]/tei:title[@type="main"]/text()
                        default return ()

    let $subtitle := switch ($source)
                        case 'gutenberg'    return $DOC//*:body/*:h4[@class="subtitle"]/text()
                        case 'textgrid'     return $DOC//tei:text[1]/tei:front[1]/tei:div[1]/tei:head[@type="h4"]/text()
                        case 'dta'          return $DOC//tei:teiHeader[1]/tei:fileDesc[1]/tei:titleStmt[1]/tei:title[@type="sub"]/text()
                        default return ()
    let $textsource := switch ($source)
                        case 'gutenberg' return 
                            <respStmt xmlns="http://www.tei-c.org/ns/1.0" xml:id="textsource-1" corresp="#availability-textsource-1">
                                <orgName>Projekt Gutenberg DE - Hille &amp; Partner</orgName>
                                <resp>
                                    <note type="remarkResponsibility">Bereitstellung der Texttranskription und
                                        Basismetadaten</note>
                                    <idno type="URLWeb">http://gutenberg.spiegel.de</idno>
                                </resp>
                            </respStmt>
                        case 'textgrid' return
                            <respStmt xmlns="http://www.tei-c.org/ns/1.0" xml:id="textsource-1" corresp="#availability-textsource-1">
                                <orgName>TextGrid</orgName>
                                <resp>
                                    <note type="remarkResponsibility"> Der annotierte Datenbestand der Digitalen
                                        Bibliothek inklusive Metadaten sowie davon einzeln zugängliche Teile
                                        sind eine Abwandlung des Datenbestandes von www.editura.de durch
                                        TextGrid und werden unter der Lizenz Creative Commons Namensnennung 3.0
                                        Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht.
                                        Die Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                        allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </note>
                                    <note type="remarkSource">Die vorliegende Textsammlung wurde im Rahmen des
                                        Forschungsprojekts TextGrid (www.textgrid.de, Förderkennzeichen:
                                        01UG1203A) mit Mitteln des BMBF (Bundesministerium für Bildung und
                                        Forschung) erworben. Wir bitten im Falle der Nachnutzung des
                                        Datenbestandes diesen Förderhinweis den Daten beizulegen.</note>
                                <idno type="URLWeb">https://textgridrep.org</idno>
                                </resp>
                            </respStmt>
                        case 'dta' return $DOC//tei:teiHeader[1]/tei:fileDesc[1]/tei:titleDesc[1]/tei:respStmt[1]
                        default return ()
    let $licence := switch ($source)
                        case 'gutenberg' return
                            <licence xmlns="http://www.tei-c.org/ns/1.0" target="http://gutenberg.spiegel.de/information"><p>nicht-kommerzielle Nutzung frei</p></licence>
                        case 'textgrid' return
                            <licence xmlns="http://www.tei-c.org/ns/1.0" target="http://creativecommons.org/licenses/by/3.0/de/"><p>CC-BY-3.0</p></licence>
                        case 'dta' return
                            <licence xmlns="http://www.tei-c.org/ns/1.0" target="http://creativecommons.org/publicdomain/zero/1.0/"><p>Dieses Werk ist gemeinfrei.</p></licence>
                        default return ()
    let $kolimoId := switch ($source)
                        case 'gutenberg'    return 'gb'  || $pos
                        case 'textgrid'     return 'tg'  || $pos
                        case 'dta'          return 'dta' || $pos
                        default return ()
    let $path := switch ($source)
(: path is used to identify a document.gb/dta comes with standard filenames, 
textgrid offers a @n attribute with a path like structure :)
                        case 'gutenberg'    return substring-after($DOC/base-uri(), $collection-gb)
                        case 'textgrid'     return $DOC/string(@n)
                        case 'dta'          return substring-after($DOC/base-uri(), $collection-dta)
                        default return ()
    let $pathBasedGenre := switch ($source)
                        case 'gutenberg'    return ()
                        case 'textgrid'     return <note type="TextgridPathBasedGenre">{tokenize($path, '/')[5]}</note>
                        case 'dta'          return ()
                        default return ()
    let $publisher := switch ($source)
                        case 'gutenberg'    return 'Hille &amp; Partner'
                        case 'textgrid'     return 'TextGrid'
                        case 'dta'          return 'Deutsches Textarchiv'
                        default return ()
    let $seriesStmt := switch ($source)
                        case 'gutenberg' return
                            <seriesStmt xmlns="http://www.tei-c.org/ns/1.0">
                                <title type="main">Projekt Gutenberg DE</title>
                                <idno type="URLXML">dvd13://{substring-after($DOC/base-uri(), 'gutenberg/')}</idno>
                                {if(exists($DOC//*:meta[@name="projectid"])) then (<idno type="GutenbergID">{$DOC//*:meta[@name="projectid"]/string(@content)}</idno>,
                                <!-- value of attribute "type" must be equal to "DTADirName", "DTAID", "EPN", "PIDCMDI", "URLCAB", "URLCatalogue", "URLHTML", "URLImages", "URLTCF", "URLText", "URLWeb", "URLXML", "URN" or "shelfmark" -->) else ()}
                            </seriesStmt>
                        case 'textgrid' return (
                            <seriesStmt xmlns="http://www.tei-c.org/ns/1.0">
                                <title type="main">TextGrid - Digitale Bibliothek</title>
                                <!-- value of attribute "type" must be equal to "DTADirName", "DTAID", "EPN", "PIDCMDI", "URLCAB", "URLCatalogue", "URLHTML", "URLImages", "URLTCF", "URLText", "URLWeb", "URLXML", "URN" or "shelfmark" -->
                            </seriesStmt>,
                            $DOC//tei:teiHeader[1]/tei:fileDesc[1]/tei:notesStmt[1])
                        (:question: nothing like a notesStmt found:)
                        case 'dta' return 
                            <seriesStmt xmlns="http://www.tei-c.org/ns/1.0">
                                <title type="main">Deutsches Textarchiv</title>
                            </seriesStmt>
                        default return ()
(: dta and textgrid comes with publicationstmt/seriesStmt :)
    let $pubStmt := switch ($source)
                        case 'gutenberg'    return 
                            <publicationStmt xmlns="http://www.tei-c.org/ns/1.0">
                                {if($DOC//*:meta[@name="publisher"]) then <publisher><name>{$DOC//*:meta[@name="publisher"]/string(@content)}</name></publisher> else ()}
                                {if($DOC//*:meta[@name="address"]) then <pubPlace>{$DOC//*:meta[@name="address"]/string(@content)}</pubPlace> else ()}
                                {if($DOC//*:meta[@name="year"]) then <date type="publication">{$DOC//*:meta[@name="year"]/string(@content)}</date> else ()}
                            </publicationStmt>
                        default return ()
    let $seriesStmt := switch ($source)
                        case 'gutenberg'    return 
                            if(exists($DOC//*:meta[@name="series"]) or exists($DOC//*:meta[@name="volume"])) then
                            <seriesStmt xmlns="http://www.tei-c.org/ns/1.0">
                                <title level="s" type="main">{$DOC//*:meta[@name="series"]/string(@content)}</title>
                                <biblScope unit="volume">{$DOC//*:meta[@name="volume"]/string(@content)}</biblScope>
                            </seriesStmt> else ()
                        default return ()
    
    let $sourceSourceDesc := switch ($source)
                                case 'gutenberg' return
                                    <sourceDesc xmlns="http://www.tei-c.org/ns/1.0">
                                        <biblFull>
                                            <titleStmt>
                                                <title level="m" type="main">{$title}</title>
                                                {if($subtitle != '') then <title level="m" type="sub">{$subtitle}</title> else ()}
                                                        {$author}
                                            </titleStmt>
                                            {$pubStmt}
                                            {$seriesStmt}
                                        </biblFull>
                                    </sourceDesc>
                                case 'textgrid' return $DOC//tei:teiHeader[1]/tei:fileDesc[1]/tei:sourceDesc[1]
                                case 'dta'      return $DOC//tei:teiHeader[1]/tei:fileDesc[1]/tei:sourceDesc[1]
                                default return ()
    let $sourceTextClass := switch ($source)
                        case 'gutenberg' return 
                            <keywords xmlns="http://www.tei-c.org/ns/1.0" scheme="http://gutenberg.spiegel.de/genre/">
                                <list>
                                    <item>
                                        {$DOC/*:head//*:meta[@name="type"]/string(@content)}
                                    </item>
                                </list>
                            </keywords>
                        case 'textgrid' return 
                            <keywords xmlns="http://www.tei-c.org/ns/1.0" scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                                <list>
                                    <item>{$DOC//tei:keywords[@scheme="http://textgrid.info/namespaces/metadata/core/2010#genre"]//text()}</item>
                                </list>
                            </keywords>
                        case 'dta' return (
                            (:maxi: someone have a look at this. especially is the "scheme" in "keywords" ok?:)
                                    (:ersetze items durch keylisten. keylisten haben immer sublisten mit items:)
                            <keywords xmlns="http://www.tei-c.org/ns/1.0" scheme="http://www.deutschestextarchiv.de/doku/klassifikation#dwds1Kategorie"> 
                                <list>
                                    <item>{$DOC//tei:classCode[@scheme="http://www.deutschestextarchiv.de/doku/klassifikation#dwds1main"]//text()}</item>
                                </list>
                            </keywords>,
                            <keywords xmlns="http://www.tei-c.org/ns/1.0" scheme="http://www.deutschestextarchiv.de/doku/klassifikation#dwds1Unterkategorie"> 
                                <list>
                                    <item>{$DOC//tei:classCode[@scheme="http://www.deutschestextarchiv.de/doku/klassifikation#dwds1sub"]//text()}</item>
                                </list>
                            </keywords>,
                            <keywords xmlns="http://www.tei-c.org/ns/1.0" scheme="http://www.deutschestextarchiv.de/doku/klassifikation#dta"> 
                                <list>
                                    <item>{$DOC//tei:classCode[@scheme="http://www.deutschestextarchiv.de/doku/klassifikation#DTACorpus"]//text()}</item>
                                </list>
                            </keywords>
                            )
(:                                    <item>{$DOC//tei:classCode[@scheme="http://www.deutschestextarchiv.de/doku/klassifikation#dwds1main"]//text()}</item>:)
(:                                    <item>{$DOC//tei:classCode[@scheme="http://www.deutschestextarchiv.de/doku/klassifikation#dwds1sub"]//text()}</item>:)
(:                                    <item>{$DOC//tei:classCode[@scheme="http://www.deutschestextarchiv.de/doku/klassifikation#DTACorpus"]//text()}</item> :)
                        default return ()

    let $creation:= switch ($source)
                        case 'gutenberg' return (
                            if(exists($DOC//*:meta[@name="firstpub"])) 
                            then <date xmlns="http://www.tei-c.org/ns/1.0" type="firstPublication">{$DOC//*:meta[@name="firstpub"]/string(@content)}</date>
                            else(),
                            if( exists($DOC//*:meta[@name="year"]) ) then <date xmlns="http://www.tei-c.org/ns/1.0" type="OriginalSourcePublication">{$DOC//*:meta[@name="year"]/string(@content)}</date> else ())
                        case 'textgrid' return <date xmlns="http://www.tei-c.org/ns/1.0" type="firstPublication">{$DOC//tei:teiHeader//tei:creation//tei:date}</date>
                        case 'dta' return <date xmlns="http://www.tei-c.org/ns/1.0" type="firstPublication">{$DOC/tei:teiHeader/tei:fileDesc[1]/tei:sourceDesc[1]/tei:biblFull[1]/tei:publicationStmt[1]/tei:date[@type="publication"]/text()}</date>
                        default return ()
    let $doc := document {
(:            processing-instruction xml-model {'href="http://media.dwds.de/dta/media/schema/basisformat.rng"'},:)
            <TEI xmlns="http://www.tei-c.org/ns/1.0">
                <teiHeader>
                    <fileDesc>
                        <titleStmt>
                            <title type="main">{$title}</title>
                            {if($subtitle != '') then <title type="sub">{$subtitle}</title> else ()}
                            {$author}
                            {$textsource}
                            <respStmt>
                                <orgName ref="http://www.textgrid.de">TextGrid</orgName>
                                <resp>
                                    <note type="remarkResponsibility">Langfristige Bereitstellung der
                                        Dokumente</note>
                                    <ref target="http://textgridrep.de"/>
                                </resp>
                            </respStmt>
                        </titleStmt>
                        <editionStmt>
                            <edition>Vollständige digitalisierte Ausgabe.</edition>
                        </editionStmt>
                        <extent>
                            <!-- DTA Daten übernehemen! -->
                            <measure type="tokens"/>
                            <measure type="types"/>
                            <measure type="characters"/>
                        </extent>
                        <publicationStmt>
                            <publisher xml:id="kolimo">
                                <!-- value of attribute "xml:id" is invalid; must be equal to "DTACorpusPublisher" -->
                                <email>jb.herrmann@phil.uni-goettingen.de</email>
                                <orgName role="project">Kolimo - Korpus der Literarischen Moderne</orgName>
                                <orgName role="hostingInstitution" xml:lang="de">Seminar für Deutsche
                                    Philologie, Georg-August-Universität Göttingen</orgName>
                                <address>
                                    <addrLine>Käthe-Hamburger-Weg 3, 37073 Göttingen</addrLine>
                                    <country>Germany</country>
                                </address>
                            </publisher>
                            <pubPlace>Göttingen</pubPlace>
                            <date type="publication">2016-06</date>
                            <availability xml:id="availability-textsource-1" corresp="#textsource-1">
                                {$licence}
                            </availability>
                            <idno type="kolimo">{$kolimoId}</idno>
                        </publicationStmt>
                        <notesStmt>
                            <!-- TextGrid offers some metadata in a path-like value of TEI/@n. I comes from the original source zeno.org. 
                                 From Gutenberg we fill this value with the physical file path from the DVD13.-->
                            <note type="SourcePath">{$path}</note>
                            {$pathBasedGenre}
                        </notesStmt>
                        {$sourceSourceDesc}
                    </fileDesc>
                    <encodingDesc>
                        <editorialDecl>
                            <p>
                                Bogensignaturen: keine Angabe; Druckfehler: ignoriert; fremdsprachliches
                                Material: keine Angabe; Geminations-/Abkürzungsstriche: keine Angabe;
                                Hervorhebungen (Antiqua, Sperrschrift, Kursive etc.): keine Angabe; i/j in
                                Fraktur: keine Angabe; I/J in Fraktur: keine Angabe; Kolumnentitel: keine
                                Angabe; Kustoden: keine Angabe; langes s (ſ): als s transkribiert;
                                Normalisierungen: stillschweigend; rundes r (&amp;#xa75b;): als r/et
                                transkribiert; Seitenumbrüche markiert: ja; Silbentrennung: keine Angabe; u/v
                                bzw. U/V: keine Angabe; Vokale mit übergest. e: als ä/ö/ü transkribiert;
                                Vollständigkeit: vollständig erfasst; Zeichensetzung: keine Angabe;
                                Zeilenumbrüche markiert: nein; </p>
                        </editorialDecl>
                    </encodingDesc>
                    <profileDesc>
                        <languageUsage>
                            <language>de-DE</language>
                            <!--  Standardwert, wird bei Bedarf geändert  -->
                        </languageUsage>
                       <creation>{$creation}</creation>
                        <textClass>
                            {$sourceTextClass}
                            <keywords scheme="http://kolimo.uni-goettingen.de/metadata#literature-non-literature">
                                <list>
                                    <item>undefined</item>
                                </list>
                            </keywords>
                            <keywords scheme="http://kolimo.uni-goettingen.de/metadata#history-of-literature">
                                <list>
                                    <item>undefined</item>
                                </list>
                            </keywords>
                        </textClass>
                    </profileDesc>
                </teiHeader>    
            </TEI>
        }
    return
        (xmldb:store($targetCollection, $filename, $doc), sm:chgrp( xs:anyURI($targetCollection || $filename), 'kolimoadmin'))
