xquery version "3.1";
(:~
 : @author Maxi Wess
 :)

declare default element namespace "http://www.tei-c.org/ns/1.0";

declare function local:check_notes_node($doc){
    let $sourcepath := replace(document-uri($doc), '/db/data/header/gb/' ,'')
    let $empty_notes := <notesStmt><!-- TextGrid offers some metadata in a path-like value of TEI/@n. I comes from the original source zeno.org.
                                 From Gutenberg we fill this value with the physical file path from the DVD13.-->
                            <note type="SourcePath">{$sourcepath}</note>
                            <note type="kolimo-date">nan</note>
                            <note type="author-gender">nan</note>
                        </notesStmt>
    return
        if (not(exists($doc//teiHeader/fileDesc/notesStmt))) then
            (update insert $empty_notes into $doc//teiHeader/fileDesc)
            else (
                if (not(exists($doc//teiHeader/fileDesc/notesStmt/note[@type='kolimo-date']))) then
                    (let $note := <note type="kolimo-date">nan</note>
                    return
                        update insert $note into $doc//teiHeader/fileDesc/notesStmt
                    ) else ()
                )
};

for $doc in collection('/db/data/kolimo/header')
return
    local:check_notes_node($doc)
