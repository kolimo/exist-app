xquery version "3.1";

(: based on the pregenerated plain text this script
 : uses R to calculate the readability scores
 : 
 : requirements: 
 :  - R
 :  - library koRpus for R
 :  - treetagger installation with language files "de"
 : 
 : this script is available for dba (admin) only
 : 
 : :)

let $pathRoot := "/opt/eXist/tools/jetty/webapps/portal/public/ID.txt/"

let $args := ("R",
                "-q",
                "-e")
let $options := ()

(:for $n in 38 to 43:)
(:let $nn := format-number($n, "00"):)
(:let $path := $pathRoot || $nn || "/":)
(:let $files := file:list($path):)
return
(:    for $file in ($files//file:file):)
    


let $readabilitydoc := doc("/db/apps/kolimo/readabilityScores-log.xml")

let $readabilities := $readabilitydoc//readability/string(@kolimoId)

let $pathRoot := "/opt/eXist/tools/jetty/webapps/portal/public/ID.txt/"
let $filenames := for $n in 1 to 43
                    let $nn := format-number($n, "00")
                    let $path := $pathRoot || $nn || "/"
                    let $files := file:list($path)
                    let $names := $files//file:file/string(@name)
                    return
                        for $n in $names return $path || $n

let $TheList :=
            for $f at $pos in $filenames
            let $kId := substring($f, 56, 12) => substring-before(".txt")
            where not( index-of($readabilities, $kId) )
            return
                $f
for $path2file at $pos in $TheList
where ($pos mod 6) = 0
return
(:    let $path2file := $path || $file/@name:)
    
    let $RScript := 'library(koRpus); readability( treetag(file.path( "' || $path2file || '" ), treetagger="manual", lang="de", TT.options=list( path="/opt/treetagger", preset="de" )), force.lang="de", index = c("Flesch", "LIX", "nWS", "RIX", "SMOG", "TRI", "Tuldava", "Wheeler.Smith"), parameters=list( Wheeler.Smith="de", Flesch="de", SMOG="de"))'
   
    let $args := ($args, $RScript)
    let $date1 as xs:string := string(process:execute( ("date", "+%H:%M:%S"), () )//line)
    let $result as node() := process:execute($args, $options)
    let $date2 as xs:string := string(process:execute( ("date", "+%H:%M:%S"), () )//line)
    
    let $ARI-data := $result//line[contains(., "(ARI)")]/(replace(substring-after(./following::line[1], ":"), " ", ""), replace(substring-after(./following::line[2], ": "), " ", ""))
    let $ARI := 
        <ARI parameters="{$ARI-data[1]}" grade="{$ARI-data[2]}" />
        
    let $ColemanFormulas-data :=  $result//line[. = "Coleman Formulas"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" (per 100 words)"), (: prononus :)
                                            ./following::line[3]/substring-after(., ": ") => substring-before(" (per 100 words)"), (: prepos :)
                                            ./following::line[4]/substring-after(., ": ") => substring-before("% cloze completions"), (: Formula 1 :)
                                            ./following::line[5]/substring-after(., ": ") => substring-before("% cloze completions"), (: Formula 2 :)
                                            ./following::line[6]/substring-after(., ": ") => substring-before("% cloze completions"), (: Formula 3:)
                                            ./following::line[7]/substring-after(., ": ") => substring-before("% cloze completions")  (: Formula 4 :)
                                            )
    let $ColemanFormulas :=
        <ColemanFormulas
            parameters="{$ColemanFormulas-data[1]}" 
            pronouns="{$ColemanFormulas-data[2]}"
            prepos="{$ColemanFormulas-data[3]}"
            formula1="{$ColemanFormulas-data[4]}"
            formula2="{$ColemanFormulas-data[5]}"
            formula3="{$ColemanFormulas-data[6]}"
            formula4="{$ColemanFormulas-data[7]}"
        />
    
    let $Coleman-Liau-data :=  $result//line[. = "Coleman-Liau"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before("%"), (: ECP :)
                                            ./following::line[3]/substring-after(., ": ") => substring-before(" "), (: Grade :)
                                            ./following::line[4]/substring-after(., ": ") => substring-before(" ")  (: Grade short:)
                                            )
    let $Coleman-Liau :=
        <Coleman-Liau
            parameters="{$Coleman-Liau-data[1]}" 
            estimatedClozePercentage="{$Coleman-Liau-data[2]}"
            grade="{$Coleman-Liau-data[3]}"
            grade-short="{$Coleman-Liau-data[4]}"
        />
        
    let $Danielson-Bryan-data :=  $result//line[. = "Danielson-Bryan"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" "), (: DB1 :)
                                            ./following::line[3]/substring-after(., ": ") => substring-before(" "), (: DB2 :)
                                            ./following::line[4]/substring-after(., ": ") => substring-before(" ")  (: Grade :)
                                            )
    let $Danielson-Bryan :=
        <Danielson-Bryan
            parameters="{$Danielson-Bryan-data[1]}" 
            db1="{$Danielson-Bryan-data[2]}"
            db2="{$Danielson-Bryan-data[3]}"
            grade="{$Danielson-Bryan-data[4]}"
        />


    let $DickesSteiwerHandformel-data :=  $result//line[. = "Dickes-Steiwer's Handformel"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" "), (: TTR :)
                                            ./following::line[3]/substring-after(., ": ") => substring-before(" ") (: score :)
                                            )
    let $DickesSteiwerHandformel :=
        <DickesSteiwerHandformel
            parameters="{$DickesSteiwerHandformel-data[1]}" 
            ttr="{$DickesSteiwerHandformel-data[2]}"
            score="{$DickesSteiwerHandformel-data[3]}"
        />

    let $EasyListeningFormula-data :=  $result//line[. = "Easy Listening Formula"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" "), (: Exsyls :)
                                            ./following::line[3]/substring-after(., ": ") => substring-before(" ") (: score :)
                                            )
    let $EasyListeningFormula :=
        <EasyListeningFormula
            parameters="{$EasyListeningFormula-data[1]}" 
            exsyls="{$EasyListeningFormula-data[2]}"
            score="{$EasyListeningFormula-data[3]}"
        />

    let $FarrJenkinsPaterson-data :=  $result//line[. = "Farr-Jenkins-Paterson"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" "), (: RE :)
                                            ./following::line[3]/substring-after(., ": ") => replace(" ", "") (: grade :)
                                            )
    let $FarrJenkinsPaterson :=
        <FarrJenkinsPaterson
            parameters="{$FarrJenkinsPaterson-data[1]}" 
            re="{$FarrJenkinsPaterson-data[2]}"
            grade="{$FarrJenkinsPaterson-data[3]}"
        />
    
    let $FleschReadingEase-data :=  $result//line[. = "Flesch Reading Ease"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" "), (: RE :)
                                            ./following::line[3]/substring-after(., ": ") => replace(" ", "") (: grade :)
                                            )
    let $FleschReadingEase :=
        <FleschReadingEase
            parameters="{$FleschReadingEase-data[1]}" 
            re="{$FleschReadingEase-data[2]}"
            grade="{$FleschReadingEase-data[3]}"
        />
        
    let $Flesch-KincaidGradeLevel-data :=  $result//line[. = "Flesch-Kincaid Grade Level"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" "), (: grade :)
                                            ./following::line[3]/substring-after(., ": ") => replace(" ", "") (: age :)
                                            )
    let $Flesch-KincaidGradeLevel :=
        <Flesch-KincaidGradeLevel
            parameters="{$Flesch-KincaidGradeLevel-data[1]}" 
            grade="{$Flesch-KincaidGradeLevel-data[2]}"
            age="{$Flesch-KincaidGradeLevel-data[3]}"
        />

    let $FOG-data :=  $result//line[. = "Gunning Frequency of Gobbledygook (FOG)"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" ") (: grade :)
                                            )
    let $FOG :=
        <FOG
            parameters="{$FOG-data[1]}" 
            grade="{$FOG-data[2]}"
        />

    let $FORCAST-data :=  $result//line[. = "FORCAST"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" "), (: grade :)
                                            ./following::line[3]/substring-after(., ": ") => substring-before(" ") (: age :)
                                            )
    let $FORCAST :=
        <FORCAST
            parameters="{$FORCAST-data[1]}" 
            grade="{$FORCAST-data[2]}"
            age="{$FORCAST-data[3]}"
        />

    let $Fucks-data :=  $result//line[. = "Fucks' Stilcharakteristik"]/(
                                            ./following::line[1]/substring-after(., ": ") => substring-before(" "), (: score :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" ") (: grade :)
                                            )
    let $Fucks :=
        <Fuck
            score="{$Fucks-data[1]}"
            grade="{$Fucks-data[2]}"
        />

    let $LinsearWrite-data :=  $result//line[. = "Linsear Write"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" "), (: easy :)
                                            ./following::line[3]/substring-after(., ": ") => substring-before(" "), (: hard :)
                                            ./following::line[4]/substring-after(., ": ") => substring-before(" ")  (: grade :)
                                            )
    let $LinsearWrite :=
        <LinsearWrite
            parameters="{$LinsearWrite-data[1]}" 
            easy="{$LinsearWrite-data[2]}"
            hard="{$LinsearWrite-data[3]}"
            grade="{$LinsearWrite-data[4]}"
        />
        
    let $LIX-data :=  $result//line[. = "Läsbarhetsindex (LIX)"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" "), (: index :)
                                            ./following::line[3]/substring-after(., ": ") => substring-before(" "), (: rating :)
                                            ./following::line[4]/substring-after(., ": ") => substring-before(" ")  (: grade :)
                                            )
    let $LIX :=
        <LIX
            parameters="{$LIX-data[1]}" 
            index="{$LIX-data[2]}"
            rating="{$LIX-data[3]}"
            grade="{$LIX-data[4]}"
        />

    let $nWS-data :=  $result//line[. = "Neue Wiener Sachtextformeln"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" "), (: nWS1 :)
                                            ./following::line[3]/substring-after(., ": ") => substring-before(" "), (: nWS2 :)
                                            ./following::line[4]/substring-after(., ": ") => substring-before(" "),  (: nWS3 :)
                                            ./following::line[5]/substring-after(., ": ") => substring-before(" ")  (: nWS4 :)
                                            )
    let $nWS :=
        <nWS
            parameters="{$nWS-data[1]}" 
            nWS1="{$nWS-data[2]}"
            nWS2="{$nWS-data[3]}"
            nWS3="{$nWS-data[4]}"
            nWS4="{$nWS-data[5]}"
        />

    let $RIX-data :=  $result//line[. = "Readability Index (RIX)"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" "), (: index :)
                                            ./following::line[3]/substring-after(., ": ") => substring-before(" ")  (: grade :)
                                            )
    let $RIX :=
        <RIX
            parameters="{$RIX-data[1]}" 
            index="{$RIX-data[2]}"
            grade="{$RIX-data[3]}"
        />

    let $SMOG-data :=  $result//line[. = "Simple Measure of Gobbledygook (SMOG)"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" "), (: grade :)
                                            ./following::line[3]/substring-after(., ": ") => substring-before(" ") (: age :)
                                            )
    let $SMOG :=
        <SMOG
            parameters="{$SMOG-data[1]}" 
            grade="{$SMOG-data[2]}"
            age="{$SMOG-data[3]}"
        />

    let $StrainIndex-data :=  $result//line[. = "Strain Index"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" ") (: index :)
                                            )
    let $StrainIndex :=
        <StrainIndex
            parameters="{$StrainIndex-data[1]}" 
            index="{$StrainIndex-data[2]}"
        />

    let $Tränkle-BailerFormulas-data :=  $result//line[. = "Tränkle-Bailer Formulas"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before("%"), (: Prepositions :)
                                            ./following::line[3]/substring-after(., ": ") => substring-before("%"), (: conjunctions :)
                                            ./following::line[4]/substring-after(., ": ") => substring-before(" "),  (: TB 1 :)
                                            ./following::line[5]/substring-after(., ": ") => substring-before(" ")  (: TB 2 :)
                                            )
    let $Tränkle-BailerFormulas :=
        <Tränkle-BailerFormulas
            parameters="{$Tränkle-BailerFormulas-data[1]}" 
            prepositions="{$Tränkle-BailerFormulas-data[2]}"
            conjunctions="{$Tränkle-BailerFormulas-data[3]}"
            tb1="{$Tränkle-BailerFormulas-data[4]}"
            tb2="{$Tränkle-BailerFormulas-data[5]}"
        />

    let $KuntzschsText-Redundanz-Index-data :=  $result//line[. = "Kuntzsch's Text-Redundanz-Index"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" "), (: short words :)
                                            ./following::line[3]/substring-after(., ": ") => substring-before(" "), (: Punctuation :)
                                            ./following::line[4]/substring-after(., ": ") => substring-before(" "),  (: Foreign :)
                                            ./following::line[5]/substring-after(., ": ") => substring-before(" ")  (: Score :)
                                            )
    let $KuntzschsText-Redundanz-Index :=
        <KuntzschsText-Redundanz-Index
            parameters="{$KuntzschsText-Redundanz-Index-data[1]}" 
            shortWords="{$KuntzschsText-Redundanz-Index-data[2]}"
            punctuation="{$KuntzschsText-Redundanz-Index-data[3]}"
            foreign="{$KuntzschsText-Redundanz-Index-data[4]}"
            score="{$KuntzschsText-Redundanz-Index-data[5]}"
        />

    let $Tuldava-data :=  $result//line[. = "Tuldava's Text Difficulty Formula"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" ") (: index :)
                                            )
    let $Tuldava :=
        <TuldavasTextDifficultyFormula
            parameters="{$Tuldava-data[1]}" 
            index="{$Tuldava-data[2]}"
        />

    let $Wheeler-Smith-data :=  $result//line[. = "Wheeler-Smith"]/(
                                            ./following::line[1]/substring-after(., ": ") => replace(" ", ""), (: parameters :)
                                            ./following::line[2]/substring-after(., ": ") => substring-before(" "), (: score :)
                                            ./following::line[3]/substring-after(., ": ") => replace(" ", "") (: grade :)
                                            )
    let $Wheeler-Smith :=
        <Wheeler-Smith
            parameters="{$Wheeler-Smith-data[1]}" 
            score="{$Wheeler-Smith-data[2]}"
            grade="{$Wheeler-Smith-data[3]}"
        />

(: 
        {$ARI}
        {$ColemanFormulas}
        {$Coleman-Liau}
        {$Danielson-Bryan}
        {$DickesSteiwerHandformel}
        {$FarrJenkinsPaterson}
        {$Flesch-KincaidGradeLevel}
        {$FOG}
        {$FORCAST}
        {$Fucks}
        {$LinsearWrite}
        {$StrainIndex}
        {$Tränkle-BailerFormulas}

 :  :)

    let $scores :=
(:    <readability kolimoId="{substring-before($file/@name, ".txt")}">:)
    <readability kolimoId="{substring($path2file, 56, 12) => substring-before(".txt")}">
    <processing startTime="{$date1}" endTime="{$date2}"/>
        {$FleschReadingEase}
        {$nWS}
        {$LIX}
        {$RIX}
        {$SMOG}
        {$KuntzschsText-Redundanz-Index}
        {$Tuldava}
        {$Wheeler-Smith}
    </readability>
        
    return
(:        $scores:)
        
        update insert $scores into doc("/db/apps/kolimo/readabilityScores-log.xml")/RScript 