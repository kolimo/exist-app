xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";

let $doc := doc("/db/apps/kolimo/readabilityScores/readabilityScores-log.xml")

let $allKolimoIds := collection("/db/data/kolimo/header")//tei:idno[@type="kolimo"]
for $kolimoId in $allKolimoIds

(:let $kolimoId := "dta47988":)

let $teiHeader := collection("/db/data/kolimo/header")//tei:idno[.=$kolimoId] (: gehe zu kolimoId :)
                    /ancestor::tei:TEI (: hole den gesamten zugehörigen TEI-Header ab :)
                    
let $scores := (: sammle die Scores ein :)
                $doc//*[@kolimoId=$kolimoId]/node()[local-name() != "processing"][local-name() != "SMOG"]

let $elements := (: bereite die Elemente vor :)
    for $score in $scores
    return
        if ($score/local-name() != "nWS") then
                element tei:measure {
                    attribute type { $score/local-name() },
                    attribute quantity {string-join(($score/string(@re), $score/string(@score), $score/string(@index)))}
                }
        else
            for $nwstype in ($score/@nWS1, $score/@nWS2, $score/@nWS3, $score/@nWS4)
            return
            element tei:measure {
                    attribute type { $nwstype/local-name() },
                    attribute quantity {string-join($nwstype)}
                }
return (: füge sie in den TEI-Header ein :)
    update insert $elements into $teiHeader/tei:teiHeader/tei:fileDesc/tei:extent
