xquery version "3.1";

let $n := 10
let $names := doc('https://de.wikipedia.org/wiki/Liste_von_Vornamen')//*:a[string(@title) = text()][matches(., "^[A-Za-z]+$")]/text()
let $countNames := count($names)

let $seq :=
    for $i in 1 to $n
    let $random := util:random($countNames)
    let $result := $names[$random]
    return
        ($result || ':' || replace(substring( util:base64-encode( generate-id($result) ), 19, 6 ), '4', util:random(99)))

for $i in $seq
    let $tok := tokenize($i, ':')
    let $user := $tok[1]
    let $pass := $tok[2]
    let $createAccount := sm:create-account($user, $pass, 'kolimouser')
return
    $tok
