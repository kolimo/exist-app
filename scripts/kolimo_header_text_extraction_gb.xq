xquery version "3.1";
(:~
 : @author Maxi Wess
 :)

declare namespace tei="http://www.tei-c.org/ns/1.0";

(:  :declare namespace default="http://www.tei-c.org/ns/1.0"; :)

declare function local:get_text_gb($doc){
    string-join($doc//body//text(), codepoints-to-string(10))
};

declare function local:get_text_tg($doc){
    string-join($doc//tei:text//text(), codepoints-to-string(10))
};

(: let $doc := doc('/db/data/kolimo/header/tg/1000-Altenberg__Peter.xml')
 : let $doc := doc('/db/data/kolimo/header/gb/about/badenbad/badenbad.xml')
 :
 : :)


for $doc in collection('/db/data/kolimo/header/gb/')
return
    let $srcpath := document-uri($doc)
    let $fname := tokenize($srcpath, '/')[last()]
    let $dstpath := replace($srcpath, '/db/data/kolimo/header', '/tmp/merging')
    let $sourcespath := replace($srcpath, '/db/data/kolimo/header', '/db/data/kolimo/sources')
    let $cleartext_document := doc($sourcespath)
    let $dstdir := replace($dstpath, $fname, '')
    let $makedir := file:mkdirs($dstdir)
    let $cleartexts := local:get_text_gb($cleartext_document)
    let $header := $doc//tei:teiHeader[1]
    let $newheader := <TEI xmlns="http://www.tei-c.org/ns/1.0">{$header, <text>{$cleartexts}</text>}</TEI>
    return
        file:serialize($newheader, $dstpath, '', false())
