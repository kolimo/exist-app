xquery version "3.1";
(:~
 : @author Maxi Wess
 :)

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare default element namespace "http://www.tei-c.org/ns/1.0";


declare function local:find_entry($col, $book_id as xs:string){
    for $doc in $col
        let $idno := $doc/tei:teiHeader/fileDesc/publicationStmt/idno[@type="kolimo"]
        return
            if (compare($book_id, $idno) eq 0) then (
                $doc)
            else ()
};

declare function local:db_pairs(){
    let $collection_dta := collection('/db/data/kolimo/header/dta')
    let $collection_gb := collection('/db/data/kolimo/header/gb')
    let $collection_tg := collection('/db/data/kolimo/header/tg')
    let $collections := ($collection_dta, $collection_gb, $collection_tg)
    (: let $collections := collection("/db/test_data") :)
    return
        ($collections//tei:teiHeader/fileDesc/publicationStmt/idno[@type="kolimo"]/text(), $collections//tei:teiHeader/fileDesc/notesStmt)
};

declare function local:lit_pairs(){
    let $source := util:binary-to-string(util:binary-doc('/db/tmp/lit_nlit2.csv'))
    return
        for $line in tokenize($source, '\n')
            let $book_id := tokenize($line, ';')[1]
            let $litornot := xs:integer(tokenize($line, ';')[2])
            let $litClass := (switch ($litornot)
                    case 1 return 'literature'
                    case 0 return 'non-literature'
                    case 9 return 'other'
                    default return "error"
                    )
            return
                ($book_id, $litClass)
};

declare function local:load_id_date_pairs(){
    let $source := util:binary-to-string(util:binary-doc('/db/tmp/kolimo_gender_dofp.tsv'))
    return
        for $line in tokenize($source, '\n')
        let $data := tokenize($line, '\t')
        (:let $book_id := tokenize($line, ';')[1]
        let $year := tokenize($line, ';')[3] :)
        return
            if (compare($data[1], 'nan') eq 0) then (
                ())
            else ($data)
};


let $date_pairs := local:load_id_date_pairs()
let $db_pairs := local:db_pairs()
return
    for $pair in $date_pairs
        let $ind := index-of($db_pairs[1], $pair[1])
        return
            ((update replace $db_pairs[$ind][2]/note[@type="kolimo-date"]/text() with $pair[3]),
            (update replace $db_pairs[$ind][2]/note[@type="author-gender"]/text() with $pair[4])
            )

(:
            <notesStmt><!-- TextGrid offers some metadata in a path-like value of TEI/@n. I comes from the original source zeno.org.
                                 From Gutenberg we fill this value with the physical file path from the DVD13.-->
                <note type="SourcePath">/abel_leibmedicus_1699.TEI-P5.xml</note>
                <note type="kolimo-date">nan</note>
                <note type="author-gender">nan</note>
                <note type="kolimo-date">nan</note>
                <note type="author-gender">nan</note>
            </notesStmt>

return
    (: count($lit_pairs) :)
    for $pair in $lit_pairs
        let $ind := index-of($db_pairs[1], $pair[1])
        return
            (update replace $db_pairs[2]/text() with $pair[2]            :)
