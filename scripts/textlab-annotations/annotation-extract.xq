xquery version "3.1";
import module namespace rede="http://kolimo.uni-goettingen.de/ns/rede" at "modules/rede.xqm";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare option exist:serialize "method=text media-type=text/plain omit-xml-declaration=yes";
let $allDocs := (
    "/db/data/kolimo/header/dta/andreas_fenitschka_1898.TEI-P5.xml",
    "/db/data/kolimo/header/dta/dery_liebe_1896.TEI-P5.xml",
    "/db/data/kolimo/header/dta/fontane_irrungen_1888.TEI-P5.xml",
    "/db/data/kolimo/header/dta/kafka_hungerkuenstler_1922.TEI-P5.xml",
    "/db/data/kolimo/header/dta/kafka_urteil_1913.TEI-P5.xml",
    "/db/data/kolimo/header/dta/raabe_stopfkuchen_1891.TEI-P5.xml",
    "/db/data/kolimo/header/dta/storm_doppelgaenger_1887.TEI-P5.xml",
    "/db/data/kolimo/header/dta/eschstruth_katz_1886.TEI-P5.xml",
    "/db/data/kolimo/header/dta/hagenauer_muspilli_1900.TEI-P5.xml",
    "/db/data/kolimo/header/dta/kafka_prozess_1925.TEI-P5.xml",
    "/db/data/kolimo/header/dta/kafka_verwandlung_1915.TEI-P5.xml",
    "/db/data/kolimo/header/dta/keyserling_beatemareile_1903.TEI-P5.xml",
    "/db/data/kolimo/header/dta/may_kurdistan_1892.TEI-P5.xml",
    "/db/data/kolimo/header/dta/preuschen_yoshiwara_1920.TEI-P5.xml",
    "/db/data/kolimo/header/dta/spyri_heidi_1880.TEI-P5.xml",
    "/db/data/kolimo/header/dta/sturza_geluebde_1905.TEI-P5.xml",
    "/db/data/kolimo/header/dta/altenberg_prodromos_1906.TEI-P5.xml",
    "/db/data/kolimo/header/dta/reventlow_dames_1913.TEI-P5.xml",
    "/db/data/kolimo/header/dta/storm_riew_1885.TEI-P5.xml",
    "/db/data/kolimo/header/dta/kraft_medizinmann_1896.TEI-P5.xml",
    "/db/data/kolimo/header/dta/heyking_erzaehlungen_1918.TEI-P5.xml",
    "/db/data/kolimo/header/dta/keller_sinngedicht_1882.TEI-P5.xml",
    "/db/data/kolimo/header/dta/polenz_buettnerbauer_1895.TEI-P5.xml",
    "/db/data/kolimo/header/dta/fontane_briest_1896.TEI-P5.xml"
)

let $return :=
    for $doc in $allDocs[1]
    let $id := doc($doc)//tei:idno[@type="kolimo"]/string()
(:    let $annotations := collection("/db/data/annotations/")//pair[@name="uri"][ends-with(., $id)]/parent::item:)
    let $TCFfilename := replace(tokenize($doc, '/')[last()], 'TEI-P5.xml', 'tcf.xml')
    let $TCFcsv := tokenize(file:read("/home/mgoebel/TCF.csv/" || $TCFfilename), '\n')
    let $tokenInTCF := $TCFcsv ! tokenize(., '\t')[1]
    let $annoSource := rede:textgrab($id)
    let $annoSourcePlaintext := string-join($annoSource//text())
    let $annoSourceTokenSeq := 
        for $token in tokenize($annoSourcePlaintext, "\W")
        where string-length($token) gt 0
        return
            $token
    let $indexOf1 := index-of($tokenInTCF, $annoSourceTokenSeq[1])[1]
    let $indexOf2 := index-of($tokenInTCF, $annoSourceTokenSeq[2])[1]
    let $indexOf3 := index-of($tokenInTCF, $annoSourceTokenSeq[3])[1]
    let $indexOf4 := index-of($tokenInTCF, $annoSourceTokenSeq[4])[1]
    let $indexOf5 := index-of($tokenInTCF, $annoSourceTokenSeq[5])[1]
    let $seq := ($indexOf1, $indexOf2, $indexOf3, $indexOf4, $indexOf5)
    
    let $test := (for $item at $pos in $seq[position() lt last()]
                    return
                        $seq[$pos+1] - $item = 1 ) = true()
    
    let $annotations := collection("/db/data/annotations/kolimouser")//pair[@name="uri"][ends-with(., $id)]/parent::item
    let $annotators := distinct-values( (for $a in $annotations return tokenize(($a/pair[@name="id"]/string()), '_')[last()]) )
    let $csv := $TCFcsv[position() >= ($indexOf1)][position() < count($annoSourceTokenSeq)]
    let $tokens := $csv ! tokenize(., '\t')[1]
    let $POS := $csv ! tokenize(., '\t')[2]
    return
        for $token at $pos in $tokens
        let $offsetStart := sum( ($tokens[position() lt $pos] ! string-length(.) ) ) + $pos - 1,
            $offsetEnd := $offsetStart + string-length( $tokens[$pos] )
(:        let $wordInAnnotationQuote := :)
(:            for $a in $annotations:)
(:                let $quote := $a/pair[@name="quote"]:)
(:                let $string := string($quote):)
(:                let $tokensInString := tokenize( $string, "\W" ):)
(:                let $index := index-of($tokensInString, $token)[1]:)
(:                where:)
(:                    $index and :)
(:                    ( $tokensInString[$index + 1] = $tokens[$pos + 1] or:)
(:                      $tokensInString[$index - 1] = $tokens[$pos - 1]:)
(:                    ):)
(:            return $a:)
        let $test := for $a in $annotations[contains(/pair[@name="quote"], $token)]
                    let $quote := tokenize(string($a/pair[@name="quote"]), '\W')
                    let $index := index-of($quote, $token)
                    return
                        for $i in $index
                        where
                            ($quote[$i + 1] = $tokens[$pos + 1]
                            or
                            $quote[$i - 1] = $tokens[$pos - 1])
                        return
                            (string($a/pair[@name="text"]), string($a/pair[@name="tags"]) )
        return
            string-join( ($token, $POS[$pos], $offsetStart, $offsetEnd, $test
(:            , serialize($wordInAnnotationQuote/pair[@name="quote"]/string()) :)
            ), '    ')

return
(   "Token  POS  OffsetStart  OffsetEnd",
    string-join(("",$return), '&#10;'))