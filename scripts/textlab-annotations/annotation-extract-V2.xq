xquery version "3.1";

import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace functx="http://www.functx.com" at "/db/system/repo/functx-1.0/functx/functx.xql";

(: prepare TCF files as sequences :)
let $TCF := "Nach    APPR    940    944#einigem    PIAT    945    952#Zögern    NN    953    959#und    KON    960    963#Schwanken    NN    964    973#von    APPR    974    977#ſeiten    NN    978    984#der    ART    985    988#Damen    NN    989    994"

let $TCFfilename := "andreas_fenitschka_1898.tcf.xml"
let $TCF := file:read("/home/mgoebel/TCF.csv/" || $TCFfilename)
let $TCFlines := tokenize($TCF, '\n')[position() lt 1111]

(:let $TCFlines := tokenize($TCF, "#"):)
    (: ggf # in \n ändern, wenn mit TCF aus filesystem gearbeitet wird :)
let $TCFtokens := $TCFlines ! tokenize(., "\s+")[1]
    (: ggf \s+ in \t ändern, wenn mit TCF aus filesystem gearbeitet wird :)
let $TCFpos := $TCFlines ! tokenize(., "\s+")[2]
    (: ggf \s+ in \t ändern, wenn mit TCF aus filesystem gearbeitet wird :)

let $annotation := (for $a in //pair[@name="uri"][ends-with(. ,"dta48513")]/parent::item
                    order by xs:integer($a//pair[@name="start"]/substring-before(substring-after(., "["), "]"))
                    return
                        $a)
let $map :=
map:new( 
    for $a in $annotation[3]
    let $quote := string($a/pair[@name = "quote"])
    let $aTokens := functx:get-matches-and-non-matches($quote , '\W' )//string(text())
    return
        for $aToken at $pos in $aTokens[string(.) != ""]
        let $index := index-of($TCFtokens, $aToken)
        let $test := console:log(($index ! " " || . ))
        return
            if ($index[2])
            then
                for $i in $index
                where  ($aTokens[$pos + 1] = $TCFtokens[$i + 1]
                        and
                        $aTokens[$pos + 2] = $TCFtokens[$i + 2]
                       ) 
                       or
                       ($aTokens[$pos - 1] = $TCFtokens[$i - 1]
                        and
                        $aTokens[$pos - 2] = $TCFtokens[$i - 2]
                       )
                return
                    map:entry($i, $a)
            else
                if($index) then map:entry($index, $a) else ()
)
return
    $TCFtokens
(:   map:keys($map):)
(:    for $i at $pos in map:keys($map):)
(:    order by $i:)
(:    return:)
(:        ($i||": "||$TCFlines[$i]|| $map($i)//pair[@name="text"]/text() ) :)

(:    string-join(( :)
(:        for $Ttoken at $pos in $TCFtokens:)
(:        return:)
(:            string-join(($Ttoken, $TCFpos[$pos], $map($pos)/pair[@name="text"]), "  "):)
(:    ), '&#10;'):)