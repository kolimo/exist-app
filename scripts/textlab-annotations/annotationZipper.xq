xquery version "3.1";

let $collection-uri := "/db/data/annotations/extract/"
let $res := xmldb:get-child-resources($collection-uri)
let $entries := 
    for $i at $pos in $res
    return
        <entry name="{$i}" type="text" method="store">{util:binary-to-string(util:binary-doc($collection-uri||$i))}</entry>
        
let $zip := compression:zip($entries, false())

return
    response:stream-binary($zip, "application/zip", "annotations.zip")