xquery version "3.1";

import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace functx="http://www.functx.com" at "/db/system/repo/functx-1.0/functx/functx.xql";

declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function local:getAnnoPerLine($annotations as node()*, $annotator as xs:string, $linenum as xs:integer) {
let $return :=  
    for $annotation in $annotations
    where   ($annotation/@user = $annotator) and
            ($annotation/item/number(@n) = $linenum)
    return
        string($annotation/@text)
let $count := count($return)
return
    switch ($count)
        case 0 return ("", "")
        case 1 return ($return, "")
        case 2 return $return
        default return ($return[1], $return[2])
(:        ("error: too many arguments in $return") :)
};
declare function local:getAnnoTagPerLine($annotations, $annotator, $linenum) {
let $return :=
    for $annotation in $annotations
    where   ($annotation/@user = $annotator) and
            ($annotation/item/number(@n) = $linenum)
    return
        string($annotation/@tags)
let $count := count($return)
return
    switch ($count)
        case 0 return ("", "")
        case 1 return ($return, "")
        case 2 return $return
        default return ($return[1], $return[2])
(:        ("error: too many arguments in $return") :)
};

let $allDocs := (
    "/db/data/kolimo/header/dta/andreas_fenitschka_1898.TEI-P5.xml",
    "/db/data/kolimo/header/dta/dery_liebe_1896.TEI-P5.xml",
    "/db/data/kolimo/header/dta/fontane_irrungen_1888.TEI-P5.xml",
    "/db/data/kolimo/header/dta/kafka_hungerkuenstler_1922.TEI-P5.xml",
    "/db/data/kolimo/header/dta/kafka_urteil_1913.TEI-P5.xml",
    "/db/data/kolimo/header/dta/raabe_stopfkuchen_1891.TEI-P5.xml",
    "/db/data/kolimo/header/dta/storm_doppelgaenger_1887.TEI-P5.xml",
    "/db/data/kolimo/header/dta/eschstruth_katz_1886.TEI-P5.xml",
    "/db/data/kolimo/header/dta/hagenauer_muspilli_1900.TEI-P5.xml",
    "/db/data/kolimo/header/dta/kafka_prozess_1925.TEI-P5.xml",
    "/db/data/kolimo/header/dta/kafka_verwandlung_1915.TEI-P5.xml",
    "/db/data/kolimo/header/dta/keyserling_beatemareile_1903.TEI-P5.xml",
    "/db/data/kolimo/header/dta/may_kurdistan_1892.TEI-P5.xml",
    "/db/data/kolimo/header/dta/preuschen_yoshiwara_1920.TEI-P5.xml",
    "/db/data/kolimo/header/dta/spyri_heidi_1880.TEI-P5.xml",
    "/db/data/kolimo/header/dta/sturza_geluebde_1905.TEI-P5.xml",
    "/db/data/kolimo/header/dta/altenberg_prodromos_1906.TEI-P5.xml",
    "/db/data/kolimo/header/dta/reventlow_dames_1913.TEI-P5.xml",
    "/db/data/kolimo/header/dta/storm_riew_1885.TEI-P5.xml",
    "/db/data/kolimo/header/dta/kraft_medizinmann_1896.TEI-P5.xml",
    "/db/data/kolimo/header/dta/heyking_erzaehlungen_1918.TEI-P5.xml",
    "/db/data/kolimo/header/dta/keller_sinngedicht_1882.TEI-P5.xml",
    "/db/data/kolimo/header/dta/polenz_buettnerbauer_1895.TEI-P5.xml",
    "/db/data/kolimo/header/dta/fontane_briest_1896.TEI-P5.xml"
)

return
    for $doc in $allDocs
(: keyserling for debug line 446 :)
(:    [12] :)
    
    let $id := doc($doc)//tei:idno[@type="kolimo"]/string(.)
    let $TCFfilename := replace(tokenize($doc, '/')[last()], 'TEI-P5.xml', 'tcf.xml')
    (:let $TCFfilename := "andreas_fenitschka_1898.tcf.xml":)
    let $TCF := file:read("/home/mgoebel/TCF.csv/" || $TCFfilename)
    let $TCFlines := tokenize($TCF, '\n')[position() lt 1111]

(:let $TCFlines := tokenize($TCF, "#"):)
    (: ggf # in \n ändern, wenn mit TCF aus filesystem gearbeitet wird :)
let $TCFtokens := $TCFlines ! 
(:functx:get-matches-and-non-matches(. , '\W' )//string(text()):)
 tokenize(., "\s+")[1]
    (: ggf \s+ in \t ändern, wenn mit TCF aus filesystem gearbeitet wird :)
let $TCFpos := $TCFlines ! tokenize(., "\s+")[2]
    (: ggf \s+ in \t ändern, wenn mit TCF aus filesystem gearbeitet wird :)

let $annotations := (for $a in //pair[@name="uri"][ends-with(. ,$id)]/parent::item
                    order by xs:integer($a//pair[@name="start"]/substring-before(substring-after(., "["), "]"))
                    return
                        $a)
let $annoMap :=
    for $annotation in $annotations
    let $user := $annotation/pair[@name="id"]/tokenize(., "_")[last()]
    let $text := replace(replace(string($annotation/pair[@name="text"]), '\s+', ''), '\n', '')
    let $tags := replace(replace(string($annotation/pair[@name="tags"]), '\s+', ''), '\n', '')
    let $quote := $annotation//pair[@name="quote"]
                            /(functx:get-matches-and-non-matches(replace(., "\s+", " "), '\W' )
                            //string(text()))[not(matches(., "\s+"))]
    let $items :=   for $word in $quote
                    return <item q="{$word}">{index-of($TCFtokens, $word)}</item>
    let $startLine :=   switch (count($items))
                        case 1 return if( count($items[1]/tokenize(., "\s+" )[. != ""]) = 1 ) then number($items[1]/tokenize(., "\s+" )[. != ""]) else ()
                        case 2 return
                            for $item in $items[1]/tokenize(., "\s+" )[. != ""]
                            where string(number($item) + 1) = $items[2]/tokenize(., "\s+" )
                            return
                                number($item)
                        case 3 return
                            for $item in $items[1]/tokenize(., "\s+" )[. != ""]
                            where   string(number($item) + 1) = $items[2]/tokenize(., "\s+" )
                                and string(number($item) + 2) = $items[3]/tokenize(., "\s+" )
                            return
                                number($item)
                        case 4 return
                            for $item in $items[1]/tokenize(., "\s+" )[. != ""]
                            where   string(number($item) + 1) = $items[2]/tokenize(., "\s+" )
                                and string(number($item) + 2) = $items[3]/tokenize(., "\s+" )
                                and string(number($item) + 3) = $items[4]/tokenize(., "\s+" )
                            return
                                number($item)
                        default return
                            for $item in $items[1]/tokenize(., "\s+" )[. != ""]
                            where   string(number($item) + 1) = $items[2]/tokenize(., "\s+" )
                                and string(number($item) + 2) = $items[3]/tokenize(., "\s+" )
                                and string(number($item) + 3) = $items[4]/tokenize(., "\s+" )
                                and string(number($item) + 4) = $items[5]/tokenize(., "\s+" )
                            return
                                number($item)
    where count($startLine) = 1
    order by $startLine
    return
        <annotation user="{$user}" text="{$text}" tags="{$tags}" startToken="{$startLine}">
        {for $i at $pos in $items[string(@q) != "’"]
        return
            <item n="{$startLine + $pos - 1}">{$i/@*}
            {comment { replace($i/text(), '\s+', ';')} }
            </item>}
        </annotation>

let $annotators := distinct-values( ($annoMap ! string(./@user)) )
let $annotators := if(count($annotators) lt 2 or $annotators[2] = "") then ($annotators[1], "Nobody") else $annotators
let $annotators := if(count($annotators) lt 2) then ("Nobody", "Nobody") else $annotators
let $csv := (
(
    "Token&#x9;POS&#x9;"||$annotators[1]||"_Annotation1&#x9;"||$annotators[1]||"_Annotation2&#x9;"||$annotators[1]||"_Tag1&#x9;"||$annotators[1]||"_Tag2&#x9;"||$annotators[2]||"_Annotation1&#x9;"||$annotators[2]||"_Annotation2&#x9;"||$annotators[2]||"_Tag1&#x9;"||$annotators[2]||"_Tag2"
),
    for $line at $pos in $TCFlines
    let $a1-texts :=    local:getAnnoPerLine($annoMap, $annotators[1], $pos)
    let $a1-tags :=     local:getAnnoTagPerLine($annoMap, $annotators[1], $pos)
    let $a2-texts :=    local:getAnnoPerLine($annoMap, $annotators[2], $pos)
    let $a2-tags :=     local:getAnnoTagPerLine($annoMap, $annotators[2], $pos)
    let $seq := ($line, $a1-texts, $a1-tags, $a2-texts, $a2-tags)
    return
        string-join($seq, "&#x9;")
(:        string-join($seq, "+++"):)
(:        for $i in $seq return ("$"||replace($i, '\s', '+')||"#"):)
)
return
    xmldb:store("/db/data/annotations/extract", $TCFfilename||".tsv", string-join($csv, "&#xa;"), "text/tab-separated-values")

(: 446 for debuging specified line from :)
(:$csv[446]:)
(:<root>{$annoMap}</root>:)