xquery version "3.1";

import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace functx="http://www.functx.com" at "/db/system/repo/functx-1.0/functx/functx.xql";


declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "text";
declare option output:media-type "text/plain";

declare function local:getAnnoPerLine($annotations, $annotator, $linenum) {
let $return :=  
    for $annotation in $annotations
    where   ($annotation/@user = $annotator) and
            ($annotation/item/@n = $linenum)
    return
        string($annotation/@text)
let $count := count($return)
return
    switch ($count)
        case 0 return ("", "")
        case 1 return ($return, "")
        case 2 return $return
        default return ("error: too many arguments in $return")
};
declare function local:getAnnoTagPerLine($annotations, $annotator, $linenum) {
let $return :=
    for $annotation in $annotations
    where   ($annotation/@user = $annotator) and
            ($annotation/item/@n = $linenum)
    return
        string($annotation/@tags)
let $count := count($return)
return
    switch ($count)
        case 0 return ("", "")
        case 1 return ($return, "")
        case 2 return $return
        default return ("error: too many arguments in $return")
};
(: prepare TCF files as sequences :)
let $TCF := "Nach    APPR    940    944#einigem    PIAT    945    952#Zögern    NN    953    959#und    KON    960    963#Schwanken    NN    964    973#von    APPR    974    977#ſeiten    NN    978    984#der    ART    985    988#Damen    NN    989    994"

let $TCFfilename := "andreas_fenitschka_1898.tcf.xml"
let $TCF := file:read("/home/mgoebel/TCF.csv/" || $TCFfilename)
let $TCFlines := tokenize($TCF, '\n')[position() lt 1111]

(:let $TCFlines := tokenize($TCF, "#"):)
    (: ggf # in \n ändern, wenn mit TCF aus filesystem gearbeitet wird :)
let $TCFtokens := $TCFlines ! 
(:functx:get-matches-and-non-matches(. , '\W' )//string(text()):)
 tokenize(., "\s+")[1]
    (: ggf \s+ in \t ändern, wenn mit TCF aus filesystem gearbeitet wird :)
let $TCFpos := $TCFlines ! tokenize(., "\s+")[2]
    (: ggf \s+ in \t ändern, wenn mit TCF aus filesystem gearbeitet wird :)

let $annotations := (for $a in //pair[@name="uri"][ends-with(. ,"dta48513")]/parent::item
                    order by xs:integer($a//pair[@name="start"]/substring-before(substring-after(., "["), "]"))
                    return
                        $a)
let $annoMap :=
    for $annotation in $annotations
    let $user := $annotation/pair[@name="id"]/tokenize(., "_")[last()]
    let $text := string($annotation/pair[@name="text"])
    let $tags := string($annotation/pair[@name="tags"])
    let $quote := $annotation//pair[@name="quote"]
                            /(functx:get-matches-and-non-matches(text(), '\W' )
                            //string(text()))[not(matches(., "\s+"))]
    let $items :=   for $word in $quote
                    return <item q="{$word}">{index-of($TCFtokens, $word)}</item>
    let $startLine :=   switch (count($items))
                        case 1 return if( count($items[1]/tokenize(., "\s+" )[. != ""]) = 1 ) then number($items[1]/tokenize(., "\s+" )[. != ""]) else ()
                        case 2 return
                            for $item in $items[1]/tokenize(., "\s+" )[. != ""]
                            where string(number($item) + 1) = $items[2]/tokenize(., "\s+" )
                            return
                                number($item)
                        case 3 return
                            for $item in $items[1]/tokenize(., "\s+" )[. != ""]
                            where   string(number($item) + 1) = $items[2]/tokenize(., "\s+" )
                                and string(number($item) + 2) = $items[3]/tokenize(., "\s+" )
                            return
                                number($item)
                        case 4 return
                            for $item in $items[1]/tokenize(., "\s+" )[. != ""]
                            where   string(number($item) + 1) = $items[2]/tokenize(., "\s+" )
                                and string(number($item) + 2) = $items[3]/tokenize(., "\s+" )
                                and string(number($item) + 3) = $items[4]/tokenize(., "\s+" )
                            return
                                number($item)
                        default return
                            for $item in $items[1]/tokenize(., "\s+" )[. != ""]
                            where   string(number($item) + 1) = $items[2]/tokenize(., "\s+" )
                                and string(number($item) + 2) = $items[3]/tokenize(., "\s+" )
                                and string(number($item) + 3) = $items[4]/tokenize(., "\s+" )
                                and string(number($item) + 4) = $items[5]/tokenize(., "\s+" )
                            return
                                number($item)
    return
        <annotation user="{$user}" text="{$text}" tags="{$tags}" startToken="{$startLine}">
        {for $i at $pos in $items return <item n="{$startLine + $pos - 1}">{$i/@*}</item>}
        </annotation>
(:let $StartLine1 := $annoMap[1]/tokenize(., "\s+" )[. != ""]:)
(:let $StartLine2 := :)
(:                    for $item in $StartLine1:)
(:                    where string(number($item) + 1) = $annoMap[2]/tokenize(., "\s+" ):)
(:                    return:)
(:                        number($item):)
(::)
(:let $annoMapFinal :=:)
(:    for $item at $pos in $annoMap:)
(:    return:)
(:        <item n="{$StartLine2 + $pos - 1}">:)
(:            {$item/@q}:)
(:        </item>:)

let $annotators := distinct-values( ($annoMap ! string(./@user)) )

let $csv := (
(
    "Token&#x9;POS&#x9;"||$annotators[1]||"_Annotation1&#x9;"||$annotators[1]||"_Annotation2&#x9;"||$annotators[1]||"_Tag1&#x9;"||$annotators[1]||"_Tag2&#x9;"||$annotators[2]||"_Annotation1&#x9;"||$annotators[2]||"_Annotation2&#x9;"||$annotators[2]||"_Tag1&#x9;"||$annotators[2]||"_Tag2"
),
    for $line at $pos in $TCFlines
    let $a1-texts := local:getAnnoPerLine($annoMap, $annotators[1], $pos)
    let $a1-tags := local:getAnnoTagPerLine($annoMap, $annotators[1], $pos)
    let $a2-texts := local:getAnnoPerLine($annoMap, $annotators[2], $pos)
    let $a2-tags := local:getAnnoTagPerLine($annoMap, $annotators[2], $pos)
    let $seq := ($a1-texts, $a1-tags, $a2-texts, $a2-tags)
    return
        string-join(($line, $seq), "&#x9;")
)
return

(:$annoMap:)
    xmldb:store("/", $TCFfilename||".tsv", string-join($csv, "&#xa;"), "text/tab-separated-values")
(:string-join(($csv), "&#xa;"):)