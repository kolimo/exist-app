xquery version "3.1";

let $collection-uris := "/db/data/annotations"

return
    for $text in collection($collection-uris)//pair[@name="text"]
    let $tok := tokenize($text, '_')
    where starts-with($text, "speech_") or starts-with($text, "thought_")
    return
        update replace $text/text() with $tok[2]||"_"||$tok[1]