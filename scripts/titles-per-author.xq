xquery version "3.1";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
declare namespace tei="http://www.tei-c.org/ns/1.0";

let $console := console:log( "START" )

let $collection-uris := '/db/data/kolimo/header/'
let $collection := collection($collection-uris)

let $authors := distinct-values($collection//tei:fileDesc/tei:titleStmt/tei:author/tei:persName/string-join(.//text()))

let $console := console:log( ("GOT DISTINCT VALUES ", $authors[1548] ) )

return
    for $a in $authors
    let $items := $collection//tei:teiHeader[//tei:fileDesc/tei:titleStmt/tei:author/tei:persName/string-join(.//text()) = $a]
    return
        count($items)

, console:log( "DONE" )