xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";

for $node in //tei:change[@who]
let $who:=string($node/@who)
let $name := sm:get-account-metadata($who, xs:anyURI('http://axschema.org/namePerson'))
let $respStmt := 
    <respStmt xmlns="http://www.tei-c.org/ns/1.0" xml:id="kolimo-staff">
        <resp>Erfassung der Metadaten</resp>
        <name xml:id="{$who}">{$name}</name>
    </respStmt>
return
    (
        update value $node/@who with '#'||$who,
        update insert $respStmt following $node/ancestor::tei:teiHeader//tei:respStmt[@xml:id="textsource-1"]
    )