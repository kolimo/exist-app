xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";

let $start as xs:integer := request:get-parameter("start", "0")
let $ids := (collection('/db/data/kolimo/header/')//tei:idno[@type="kolimo"])[position() gt $start][position() lt ($start + 100)]
let $headers := $ids ! ./ancestor::tei:teiHeader

return
 for $id in $ids
 let $id-str := string($id)
 let $GND := $ids/ancestor::tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:persName/substring-after(@ref, "http://d-nb.info/gnd/")
 let $GND := if($GND = "") then "NOGND" else $GND
 let $title := $id/ancestor::tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[@type="main"]
 let $author := string-join($id/ancestor::tei:teiHeader/tei:fileDesc/tei:titleStmt//tei:author//text(), " " )
 let $seq := ($id-str, $GND, $author, $title)
 return
    string-join( $seq, "&#x9;" )