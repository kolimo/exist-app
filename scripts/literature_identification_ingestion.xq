xquery version "3.1";
(:~
 : @author Maxi Wess
 :)

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare default element namespace "http://www.tei-c.org/ns/1.0";


declare function local:find_entry($col, $book_id as xs:string){
    for $doc in $col
        let $idno := $doc/tei:teiHeader/fileDesc/publicationStmt/idno[@type="kolimo"]
        return
            if (compare($book_id, $idno) eq 0) then (
                $doc)
            else ()
};

declare function local:db_pairs(){
    let $collection_dta := collection('/db/data/kolimo/header/dta')
    let $collection_gb := collection('/db/data/kolimo/header/gb')
    let $collection_tg := collection('/db/data/kolimo/header/tg')
    let $collections := ($collection_dta, $collection_gb, $collection_tg)
    (: let $collections := collection("/db/test_data") :)
    return
        ($collections//tei:teiHeader/fileDesc/publicationStmt/idno[@type="kolimo"]/text(), $collections//tei:teiHeader/profileDesc/textClass/keywords[@scheme="http://kolimo.uni-goettingen.de/metadata#literature-non-literature"]/list/item)
};

declare function local:lit_pairs(){
    let $source := util:binary-to-string(util:binary-doc('/db/tmp/lit_nlit2.csv'))
    return
        for $line in tokenize($source, '\n')
            let $book_id := tokenize($line, ';')[1]
            let $litornot := xs:integer(tokenize($line, ';')[2])
            let $litClass := (switch ($litornot)
                    case 1 return 'literature'
                    case 0 return 'non-literature'
                    case 9 return 'other'
                    default return "error"
                    )
            return
                ($book_id, $litClass)
};


let $db_pairs := local:db_pairs()
let $lit_pairs := local:lit_pairs()
return
    count($lit_pairs)
    (:for $pair in $lit_pairs
        let $ind := index-of($db_pairs[1], $pair[1])
        return
            (update replace $db_pairs[2]/text() with $pair[2])
      :)
(:
let $source := util:binary-to-string(util:binary-doc('/db/tmp/lit_nlit2.csv'))
let $collection_dta := collection('/db/data/kolimo/sources/dta')
let $collection_gb := collection('/db/data/kolimo/sources/gb')
let $collection_tg := collection('/db/data/kolimo/sources/tg')

let $gb_prefix := 'gb'
let $tg_prefix := 'tg'
let $dta_prefix := 'dta'


for $line in tokenize($source, '\n')
    let $book_id := tokenize($line, ';')[1]
    let $litornot := xs:integer(tokenize($line, ';')[2])
    let $litClass := (switch ($litornot)
                        case 1 return 'literature'
                        case 0 return 'non-literature'
                        case 9 return 'other'
                        default return "error"
                        )
    let $current_collection := if (starts-with($book_id, $gb_prefix)) then $collection_gb else if
                    (starts-with($book_id, $dta_prefix)) then $collection_dta else if
                    (starts-with($book_id, $tg_prefix)) then $collection_tg else
                ()
    let $teiHeader := $current_collection//tei:teiHeader/fileDesc/publicationStmt/idno[. = $book_id]/ancestor-or-self::tei:teiHeader
    return
        ($teiHeader//tei:teiHeader/fileDesc/titleStmt/title[@type="main"])
       (: (update replace $teiHeader/profileDesc/textClass/keywords[@scheme="http://kolimo.uni-goettingen.de/metadata#literature-non-literature"]/list/item with $litClass) :)


:)
