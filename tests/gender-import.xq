xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";

let $doc := util:binary-doc("/db/tmp/kolimo_gender_dofp.tsv") => util:base64-decode()

let $lines := tokenize($doc, "\n")

for $line in $lines[1]
(: splitte die Daten aus der Tabelle auf :)
let $token := tokenize($line, "\s")[.!=""]
let $kolimoId := $token[1] (: unused :)
let $gndId := $token[2]
let $date := $token[3]
let $gender := $token[4]

(: hole die TEI-Header für diese Autorin :)
let $documentsPerAuthor := collection("/db/data/kolimo/header")//tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/tei:persName[substring-after(@ref, "http://d-nb.info/gnd/") = $gndId]/ancestor::tei:TEI

return
    ($token, count( $documentsPerAuthor ), $documentsPerAuthor//tei:notesStmt)