xquery version "3.1";
(:~
 : @author Maxi Wess
:)

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare option output:method "html5";
declare option output:media-type "text/html";



    for $header in collection("/db/data/kolimo/header")//tei:teiHeader
    let $pubDate := $header//tei:sourceDesc//tei:publicationStmt//tei:date
    let $date := [
        $pubDate[@notBefore < "1700"],
        $pubDate[@when < "1700"],
        $pubDate[not(@when|@notBefore)][. < "1700"],
        $header//tei:note[@type="kolimo-date"][. < "1700"]
    ]
    let $useDate := $date?1
    order by $useDate ascending
    return
        if (array:size($date) > 0) then

            (   util:document-name($header)

                   ( for $d in $date?*
                    return
                        ($d/@when/string(), $d/text())
             ))

        else
            ()
