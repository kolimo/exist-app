xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare default element namespace "http://www.tei-c.org/ns/1.0";
import module namespace functx="http://www.functx.com" at "/db/system/repo/functx-1.0/functx/functx.xql";

let $asd := functx:get-matches("um 1900-1234", "\d{4}")
let $seq :=     for $v in $asd
                order by -1*string-length($v)
                    return $v
return
    $seq[1]
    

        