xquery version "3.1";
(:~
: This is the main module to all kolimo scripts and apps
: @author Mathias Göbel
: @version 1.0
: @see http://kolimo.uni-goettingen.de
:
:)
module namespace kolimo="http://kolimo.de/";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace templates="http://exist-db.org/xquery/templates" ;
import module namespace config="http://kolimo.de/config" at "config.xqm";

(:~
 : Generates a table template to be filled by ajax calls at
 : /json/gesamtliste.json
 :
 : @param $table takes the filename or other string indicator for the table to be prepared
 : @return html:table as node()
 : @deprecated at release
 :  :)
declare function kolimo:ajaxtable($node as node(), $model as map(*), $table as xs:string) as element(table){

let $doc := doc('/db/apps/kolimo/data/'||$table)
let $unusedNames := ('InKafkasBibliothek', 'Standort', 'InLiterarischesUmfeld', 'InKafkasBibliothek', 'Expertenrating', 'edit')
let $heads := distinct-values(($doc/*/entry)[1]/*[not(local-name() = $unusedNames)]/local-name())
let $head :=
    <tr>
    <th><span>ID</span></th>
	{ $heads ! <th><span>{
	    switch(.)
	        case 'Erscheinungsjahr' return "Erscheinungs- jahr"
	        case 'Rating' return 'Lit.-Geschichte'
	        case 'Kategorie' return 'Kategorie (Gattung)'
	        case 'Unterkategorie' return 'Unterkategorie (Untergattung)'
	        default return . }</span></th>}
	</tr>
return
<table id="{substring-before($table, '.')}" class="table table-hover" style="clear: both">
	<thead>
		{$head}
	</thead>
	<tfoot>
		{$head}
	</tfoot>
</table>
};

(:~
 : Old static and complete table generator
 : delivers all rows in a single html page and makes any JS unusable
 : @deprecated
 :   :)
declare function kolimo:table($node as node(), $model as map(*), $table as xs:string){
let $doc := doc('/db/apps/kolimo/data/'||$table)
let $unusedNames := ('InKafkasBibliothek', 'Standort', 'InLiterarischesUmfeld', 'InKafkasBibliothek', 'Expertenrating', 'edit')
let $heads := distinct-values(($doc/*/entry)[1]/*[not(local-name() = $unusedNames)]/local-name())
return
<table id="{substring-before($table, '.')}" class="table table-hover" style="clear: both">
	<thead>
		<tr>
		    <th><span>ID</span></th>
	    	{ $heads ! <th><span>{
	    	    switch(.)
	    	        case 'Erscheinungsjahr' return "Erscheinungs- jahr"
	    	        case 'Rating' return 'Lit.-Geschichte'
	    	        case 'Kategorie' return 'Kategorie (Gattung)'
	    	        case 'Unterkategorie' return 'Unterkategorie (Untergattung)'
	    	        default return . }</span></th>}
		</tr>
	</thead>
	<tbody>
	    {
	        for $entry at $pos in $doc//entry
(:	        where $pos lt 51:)
	        return
	        <tr>
	            <td class="kolimoid">{ substring-after($entry/@id, 'kolimo') }</td>
	            { for $item at $posi in $entry/*[not(local-name() = $unusedNames)] return <td class="makeedit"><span class="{lower-case($heads[$posi])} cell" data-pk="{$entry/@id}" data-name="{lower-case($heads[$posi])}" data-url="{substring-before($table, '.')}.post.html">{ $item/string() }</span><span class="makeediticon"><i class="fa fa-pencil" aria-hidden="true"></i></span></td> }
	        </tr>
	    }
    </tbody>
</table>
};

(:~
 : Template for using a data counter as a nice visualization gimmick
 : @data-to and text() should be updated.
 : @return html:span
 :)
declare function kolimo:countEntries($node as node(), $model as map(*)){
    <span class="timer" data-from="0" data-to="{count( collection( '/db/data/kolimo/header' )//tei:teiHeader )}" data-speed="1500" data-refresh-interval="100">
        {count( collection( "/db/data/kolimo/header" )//tei:teiHeader )}
    </span>

};

(:~
 : Template for using a data counter as a nice visualization gimmick
 : @data-to and text() should be updated.
 : @return html:span
 :)
declare function kolimo:countAuthors($node as node(), $model as map(*)){
    <span class="timer" data-from="0" data-to="2029" data-speed="1500" data-refresh-interval="100">
        2029
    </span>
};

(:~
 : Delivers (unparsed) all nodes under *:body
 : Browser may interpret some parts of the output, but leave out others. use with caution!
 :
 : @param
 : @param
 : @param Kolimo-ID of a document
 : @return node()*
 :)
declare function kolimo:text($node as node(), $model as map(*), $id as xs:string) as node()* {
    let $uri := collection( '/db/data/kolimo/header' )//tei:idno[. = $id]/base-uri()
    let $source := doc( replace( $uri, 'kolimo/header', 'kolimo/sources' ) )
    return
        ($source//body/*, $source//tei:body/*)
};

(:~
 : Textview with POS-Tags.
 :
 : @param
 : @param
 : @param Kolimo-ID of a document
 : @return node()*
 :)
declare function kolimo:textview($node as node(), $model as map(*), $id as xs:string){
let $teiHeader := collection( '/db/data/kolimo/header/' )/range:field-eq('idno', $id)
let $pathToHeader := $teiHeader/string(base-uri())
let $tei := if(starts-with($id, 'tg')) then doc( replace($pathToHeader, 'header/tg', 'textgrid-tei') ) else
                doc( replace($pathToHeader, 'header/gb', 'gutenberg') )
let $seq := kolimo:tokenize( ($tei//*:body//*:p)[1] )

let $txt := util:base64-decode( string( util:binary-doc( '/db/data/kolimo/tagged/aufklaerung/db_data_kolimo_gutenberg_kafka_verwandl_verwa001-p1.txt' ) ) )
let $seqPOS := tokenize($txt, '\n')[position() gt 1]

let $text:=
    for $w at $pos in $seq
    let $stts := tokenize($seqPOS[$pos], '\t')[2]

    return
        (element xhtml:div {
            attribute class {'pos'},
            element xhtml:span {
                attribute class {'word'},
                $w},
            element xhtml:br {},
            element xhtml:span {
                attribute class {'stts', $stts},
                $stts
                }
            }
            , ' ')
return
    (element xhtml:h3 { ($teiHeader//tei:title)[1]//text() ,  ' by ', string-join(($teiHeader//tei:author)[1]//text(), ' ') },
    $text)
};


declare function kolimo:tokenize( $p as element(*) ){
let $seqTEI := tokenize( replace(string-join($p), "'\w+", ' $0'), ' ')
let $seqTEI := let $pattern := ';' return for $i in $seqTEI return if(matches($i, $pattern||'$')) then (substring-before($i, $pattern), $pattern) else $i
let $seqTEI := let $pattern := ',' return for $i in $seqTEI return if(matches($i, $pattern||'$')) then (substring-before($i, $pattern), $pattern) else $i
let $seqTEI := let $pattern := ':' return for $i in $seqTEI return if(matches($i, $pattern||'$')) then (substring-before($i, $pattern), $pattern) else $i
let $seqTEI := let $pattern := '.' return for $i in $seqTEI return if(matches($i, '\'||$pattern||'$')) then (substring-before($i, $pattern), $pattern) else $i
return $seqTEI
};

declare function kolimo:returnQueryString($node as node(), $model as map(*)) {
    request:get-query-string()
};
