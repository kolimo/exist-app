xquery version "3.1";

module namespace kapi="https://kolimo.uni-goettingen.de/api";
import module namespace rest="http://exquery.org/ns/restxq";


declare namespace xhtml="http://www.w3.org/1999/xhtml";
declare namespace tei="http://www.tei-c.org/ns/1.0";

declare variable $kapi:header-collection := collection("/db/data/kolimo/header");

declare
    %rest:GET
    %rest:path("/kolimo/info.xml")
    %rest:produces("application/xml")
function kapi:api-description() {
    rest:resource-functions()
    
};

declare
    %rest:GET
    %rest:path("/kolimo/tei/{$id}/xml")
    %rest:produces("application/xml")
function kapi:api-description($id) {
if(not(matches($id, "(^gb|^dta|^tg)\d+")))
then error( QName("https://kolimo.uni-goettingen.de/api", "ID"), "unknown ID." )
else
    let $idno := $kapi:header-collection//tei:idno[@type="kolimo"][.=$id]
    let $base-uri := $idno/base-uri()
    let $header := $idno/ancestor::tei:teiHeader
    
    let $text := doc( replace($base-uri, "/header/", "/sources/") )/html/body
return
    <tei:TEI>
    {$header}
    {$text}
    </tei:TEI>
};

declare
    %rest:GET
    %rest:path("/kolimo/ids")
    %rest:produces("application/xml")
function kapi:ids() {
<kolimo>
    {for $i in $kapi:header-collection//tei:idno[@type="kolimo"]
        return
            <item>
                <id>{$i/text()}</id>
                <author>{$i/ancestor::tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:author/*}</author>
                <title>{string($i/ancestor::tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title[1])}</title>
                <services>
                    <service name="get-combined-tei" url="/kolimo/tei/{$i/text()}/xml"/>
                </services>
            </item>
    }
</kolimo>
};
