xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

declare option output:method "json";
declare option output:media-type "application/json";

declare variable $Kid := request:get-parameter("kolimoId", "gb9166");

let $header:= collection("/db/data/kolimo/header")//tei:idno[.=$Kid]/ancestor::tei:TEI
let $source := doc( replace( $header/base-uri(), "header", "sources" ) )/*

return
    <kolimo>{(
        $header,
        $source
    )}</kolimo>
