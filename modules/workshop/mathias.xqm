xquery version "3.1";
module namespace kolimo-mathias="http://kolimo.uni-goettingen.de/mathias";

declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function kolimo-mathias:magic($node as node(), $model as map(*)) {
    distinct-values(
        collection("/db/data/kolimo/header")//tei:textClass//string(.)
    )
};