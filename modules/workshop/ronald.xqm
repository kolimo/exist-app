xquery version "3.1";

module namespace kolimo-ronald="http://kolimo.uni-goettingen.de/ronald";
declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function kolimo-ronald:magic($node as node(), $model as map(*)) {
distinct-values((collection('/db/data/kolimo/header')//tei:textClass//tei:item))
};

declare function kolimo-ronald:magic_2($node as node(), $model as map(*)) {
for $creationdate in collection('/db/data/kolimo/header')//tei:creation
let $date := $creationdate/tei:date[@type = "firstPublication"]
where number($date) gt 1800
return <li>Date: {number($date)}</li> 
};