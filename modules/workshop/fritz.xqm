xquery version "3.1";
    
module namespace kolimo-fritz="http://kolimo.uni-goettingen.de/fritz";
declare namespace tei="http://www.tei-c.org/ns/1.0";



(:~
 : @author Fritz
 : @return all root elemetents inside the collection
 :  
 :  :)
declare function kolimo-fritz:collection() {
    collection("/db/workshop")
};