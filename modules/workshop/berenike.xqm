xquery version "3.1";
    
module namespace kolimo-berenike="http://kolimo.uni-goettingen.de/berenike";
declare namespace tei="http://www.tei-c.org/ns/1.0";

(:~
 : returns an ordered list of flesch scores
 : 
 : @author Berenike
 : @param node
 : @param model
 : @return html:li*
 :  
 :  :)
declare function kolimo-berenike:magic($node as node(), $model as map(*)){
 for $i in kolimo-berenike:collection()
let $title := string(($i//tei:title)[1])
let $flesch := number($i//tei:extent/tei:measure [@type="readability-flesch"])
 order by $flesch, $title
 return <li>Flesch:  {(string($flesch), $title)}
   </li>
};

(:~
 : tell what this function does!
 : 
 : @author Berenike
 : @return all root elemetents inside the collection
 :  
 :  :)
declare function kolimo-berenike:collection() {
    collection("/db/workshop")
};