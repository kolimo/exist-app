xquery version "3.1";
module namespace kolimo-simone="http://kolimo.uni-goettingen.de/simone";

declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function kolimo-simone:magic( $node as node(), $model as map(*) ) as map(*) {
    let $results := 
        collection("/db/data/kolimo/header")//tei:measure[@type="readability-flesch"]
    
    return
        map { "results" := $results }
    
};

declare function kolimo-simone:titles($node as node(), $model as map(*),
    $minScore as xs:integer*, 
    $maxScore as xs:integer*)
{
    
    if( empty($minScore) or empty($maxScore) ) then
        for $r in 
        collection("/db/data/kolimo/header")
            //tei:measure
                [@type="readability-flesch"]
 
    return
        (   string( ($r/ancestor::tei:teiHeader//tei:title)[1] ),
            string($r)
        )
    else
    
    for $r in 
        collection("/db/data/kolimo/header")
            //tei:measure
                [@type="readability-flesch"]
                [number(.) gt $minScore]
                [number(.) lt $maxScore]
 
    return
        (   string( ($r/ancestor::tei:teiHeader//tei:title)[1] ),
            string($r)
        )
};
