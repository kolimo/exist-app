xquery version "3.1";


module namespace kolimo-maxi="http://kolimo.uni-goettingen.de/maxi";
declare namespace tei="http://www.tei-c.org/ns/1.0";


declare function kolimo-maxi:magic($node as node(), $model as map(*)) {
    distinct-values(collection('db/data/kolimo/header')//tei:teiHeader)
};