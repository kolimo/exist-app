xquery version "3.1";

module namespace kolimo-markus="http://kolimo.uni-goettingen.de/markus";
declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function kolimo-markus:magic($node as node(), $model as map(*)) {
    distinct-values(
        collection("/db/data/kolimo/header/")//tei:date[@type="firstPublication"])
        
};