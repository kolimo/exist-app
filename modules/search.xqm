xquery version "3.1";

module namespace ksearch="http://kolimo.uni-goettingen.de/ns/search";
declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function ksearch:search($node as node(), $model as map(*)) {
    let $queryString := request:get-query-string()
    let $queries := tokenize( $queryString, '&amp;' )
    let $results :=
                    for $query in $queries
                    let $parameter := tokenize($query, '=')[1]
                    let $term := tokenize($query, '=')[2]
                    return
                        ksearch:perform($parameter, $term)

    let $count := count( $queries )
    let $results := if( $count gt 1 )
                    then
                        let $results := $results ! string(.)
                        return
                        $results[index-of($results, .)[ $count ]] ! ksearch:source(.)
                    else
                        $results
    return
        map { 'results' := $results }
};

declare function ksearch:perform($parameter, $term) {
let $term := xmldb:decode($term) return
switch($parameter)
    case 'author' return ksearch:author($term)
    case 's' return ksearch:source($term)
    case 'title' return ksearch:title($term)
    case 'date' return ksearch:date($term)
    case 'fleschgt' return ksearch:fleschgt($term)
    default return ()
};

declare function ksearch:hits($node as node(), $model as map(*)) {
    count( $model('results') )
};

declare function ksearch:results($node as node(), $model as map(*)) {
let $username := request:get-attribute("org.exist.login.user")
return
        <ol id="results" xmlns="http://www.w3.org/1999/xhtml">
        {
(:            for $result in $results[position() lt 101]:)
            for $result in $model('results')
(:            return <li>{$result}</li>:)
            let $id := $result
            let $base := $result/base-uri()
            let $result := 
                    doc( $base )
            let $author := ($result//tei:author)[1]
            let $gndId := $author//tei:persName/string(@ref)
            let $auth := string-join( (($result//tei:author)[1]//text()), ' ' )
            let $authShort := if( string-length($auth) gt 20 ) then substring($auth, 0, 20) || '…' else $auth
            let $title := string( ($result//tei:title)[1] )
            let $titleShort := if( string-length($title) gt 20 ) then substring($title, 0, 20) || '…' else $title
            let $date := $result//tei:date[@type="firstPublication"]/string(.)
            let $dateUnreliable := if($date = ()) then $result//tei:date/string(.) else ()
            return
                <li>
                    <span class="badge kolimoId" title="{$result/base-uri()}">{$id}</span>
                    <span class="badge badge-primary gndId {
                        if( $result//tei:change[contains(., 'GNDIDCONFIRM')]) then 'confirmed' else ()}">{
                        if($gndId != '') then
                            if(count( tokenize($gndId, ' ') ) gt 1) then 'multiple val'
                            else <a href="{$gndId}">{substring-after($gndId, '/gnd/')}</a>
                        else ' no value '
                    }</span>
                    <span class="author" title="{$auth}">{$authShort}</span>
                    <span class="title" title="{$title}">{$titleShort}</span>
                    <span class="{if($dateUnreliable != ()) then 'date badge badge-danger unreliable' else 'date'}" title="Erscheinungsdatum">
                        {($date, string-join($dateUnreliable, ';'))}
                    </span>
                    <span class="text"> <a href="text.html?id={$id}" target="_blank"> <i class="fa fa-file-text-o" aria-hidden="true"></i> </a> </span>
                    {
                        if(string($username) = "") then () else if(sm:get-user-groups( $username ) = ('kolimoadmin', 'dba') ) then (
                    <span class="edit"> <a href="edit.html?id={$id}" target="_blank"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a> </span>,
                    if(sm:is-dba($username)) then
                        <span class="header">  <a href="http://kolimo.uni-goettingen.de:8080/exist/apps/eXide/index.html?open={$base}" title="edit TEI-Header (Goenet only)"> <i class="fa fa-file-code-o" aria-hidden="true"></i> </a> </span>
                        else ())
                    else ()}
                </li>
        }
        </ol>
};

declare function ksearch:source($term as xs:string) {
    for $i in collection('/db/data/kolimo/header')//tei:idno[starts-with(., $term)] return $i
};

declare function ksearch:author($term as xs:string) {
if ( contains($term, 'surname:') ) then
    let $term := substring-after($term, 'surname:') return
    for $item in collection('/db/data/kolimo/header')//tei:author[contains(., $term)]
    where contains($item//tei:surname, $term)
    return
        ($item/following::tei:idno[@type="kolimo"])[1]
else
    for $item in collection('/db/data/kolimo/header')//tei:author[contains(., $term)]
    return
        ($item/following::tei:idno[@type="kolimo"])[1]
};
declare function ksearch:title($term as xs:string) {
    for $item in collection('/db/data/kolimo/header')//tei:title[contains(., $term)]
    return
        ($item/following::tei:idno[@type="kolimo"])[1]
};

declare function ksearch:date($term as xs:string) {
    let $years := tokenize( $term, '-' ) ! xs:integer(number(.))
    let $years := for $i in $years[1] to $years[2] return xs:integer($i)
    return
    for $item in collection('/db/data/kolimo/header')//tei:note[@type="kolimo-date"][. != "nan"][. = $years]
    return
         ($item/following::tei:idno[@type="kolimo"])[1]
};

declare function ksearch:fleschgt($term) {
    for $item in collection('/db/data/kolimo/header')//tei:measure[@type="readability-flesch"][. gt $term]
    return
        ($item/following::tei:idno[@type="kolimo"])[1]
};
