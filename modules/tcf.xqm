xquery version "3.1";
module namespace kolimotcf="http://kolimo.uni-goettingen.de/tcf";


declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace tc="http://www.dspin.de/data/textcorpus";


declare function kolimotcf:tcftext($node as node(), $model as map(*), $id as xs:string) {
    let $base := collection( "/db/data/kolimo/header" )//tei:idno[@type="kolimo"][. = $id]/base-uri()
    let $tcf := doc( replace($base, "header", "tcf") )
    return
        <div class="tcf">
            {for $token in ($tcf//tc:tokens/tc:token)[100]
            let $id := $token/@ID
                return
                    <div class="work">
                        <div class="token">{$token}</div>
                        {for $tag in $tcf//tc:tag[@tokenIDs = $id] return 
                            <div class="stts">{$tag}</div>
                        }
                        <div class="lemma">{$tcf//tc:lemma[@tokenIDs=$token]}</div>
                    </div>
            }
        </div>
};
