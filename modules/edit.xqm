xquery version "3.1";

module namespace kedit="http://kolimo.uni-goettingen.de/ns/edit";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace tgpnd="http://textgrid.info/namespaces/vocabularies/pnd";

declare function kedit:entry($node as node(), $model as map(*)) as map(*) {
    let $id := request:get-parameter('id', '')
    let $doc := doc( collection('/db/data/kolimo/header')//tei:idno[. = $id]/base-uri() )
    let $paras := request:get-parameter-names()[. != 'id']
(: process POST data :)
    let $test := console:log( (
        for $para in $paras
            let $val := request:get-parameter($para, '')
            where $val != ''
            return
                $para || ":" || $val
        )
        )
    let $parameter := 
                    for $para in $paras
                    where request:get-parameter($para, '') != ''
                    return $para
    let $processing :=
            for $para in $parameter
            let $val := request:get-parameter($para, '')
            let $param := if( $para = 'gndIdOverride' ) then 'gndIdConfirm' else $para
            return
                if($para = 'gndIdConfirm' and request:get-parameter('gndIdOverride', '') != '' ) then () else
                kedit:updateDb( $doc, $param, $val )

    let $doc := doc( collection('/db/data/kolimo/header')//tei:idno[. = $id]/base-uri() )
    return
        ( map { 
            'forename' :=   (($doc//tei:author)[1]//tei:forename/string(.))[1],
            'surname' :=    (($doc//tei:author)[1]//tei:surname/string(.))[1],
            'nameLink' :=   (($doc//tei:author)[1]//tei:nameLink/string(.))[1],
            'addName' :=    (($doc//tei:author)[1]//tei:addName/string(.))[1],
            'genName' :=    (($doc//tei:author)[1]//tei:genName/string(.))[1],
            'roleName' :=   (($doc//tei:author)[1]//tei:roleName/string(.))[1],
            'gndId' := (($doc//tei:author)[1]/tei:persName/string(@ref))[1],
            'gndIdConfirmed' := (if( $doc//tei:change/contains(., 'GNDIDCONFI') = true() ) then true() else false())
        } )
};

declare function kedit:edit($node as node(), $model as map(*), $edit as xs:string) {
switch ($edit)
    case "gndId" return
        if($model($edit) = "")
        then
            let $refUrl :='http://ref.dariah.eu/beta/pndsearch/pndquery.xql?ln='||encode-for-uri((string-join( ($model('surname'), $model('forename')) , ',')) ),
                $doc := httpclient:get(xs:anyURI($refUrl), false(), ()),
                $count := count( $doc//tgpnd:person ),
                $doc := if( $count gt 10 ) 
                        then httpclient:get( xs:anyURI($refUrl || encode-for-uri(' and dnb.sw=Schriftsteller')), false(), () )
                        else $doc
                return
                    kedit:gndConfim( $doc//tgpnd:response )
        else if(contains($model($edit), ' '))
        then
            (<p>multiple items found. refine the selection or enter a correct value.</p>,
            <div class="form-group">
		        <label>Select one of the following</label>
                {for $id at $pos in tokenize($model($edit), ' ')
                let $id := substring-after($id, '/gnd/')
                return
                    <div class="radio">
                        <input type="radio" value="{ $id }" id="gndId{ $pos }" name="gndIdConfirm"/>
                        <label for="gndId{ $pos }">
            				<a href="http://d-nb.info/gnd/{ $id }" class="gndLink" target="_blank"> { $id }</a></label>
                    </div>}
            </div>,
            <div class="form-group">
                <input type="gndIdnew" placeholder="new GND ID" name="gndIdOverride" id="gndoverride" class="form-control"/>
            </div>
            )
        else
            (<p>The automatic search found a single item. Confim this id or enter a correct value.</p>,
            <div><a href="{ $model($edit) }" class="gndLink" target="_blank"> { $model($edit) } </a></div>,
            <div class="onoffswitch onoffswitch-success">
    			<input type="checkbox" id="gndConfim1" class="onoffswitch-checkbox" name="gndIdConfirm" value="{ substring-after($model($edit), '/gnd/') }">
    			    {if($model('gndIdConfirmed')) then attribute checked {} else ()}
    			</input>
    			<label for="gndConfim1" class="onoffswitch-label"><div class="onoffswitch-inner"></div><div class="onoffswitch-switch"></div></label>
    		</div>,
            <div class="form-group">
        		<input type="gndIdnew" placeholder="new GND ID" name="gndIdOverride" id="gndoverride" class="form-control"/>
        	</div>
    		)
    default return <input type="text" placeholder="{ $model($edit) }" id="{ $edit }" name="{ $edit }" class="form-control"/>
};

declare function kedit:gndConfim( $item ) {
    let $test := console:log( $item ) return
if( count( $item//tgpnd:person ) = 0 ) 
    then (
        <p>Kolimo was not able to autodetect a data set. Please try a <a href="https://portal.dnb.de/opac.htm?method=simpleSearch&amp;query=Suchwort" target="_blank">manual search</a> and provide the result or confirm that there is no entry in the database.</p>,
        <div class="form-group">
        	<input type="gndIdnew" placeholder="new GND ID" name="gndIdOverride" id="gndoverride" class="form-control"/>
        </div>
        )
else if( count( $item//tgpnd:person ) = 1 ) 
    then (
        <p>Please confirm the following ID or try a <a href="https://portal.dnb.de/opac.htm?method=simpleSearch&amp;query=Suchwort" target="_blank">manual search</a> and provide the result.</p>,
        <div class="radio">
            <input type="radio" value="{ $item//tgpnd:link/substring-after(@target, '/gnd/') }" id="radio1" name="gndIdConfirm"/>
            <label for="radio1">
                <a href="{ $item//tgpnd:link/string(@target) }" class="gndLink" target="_blank"> { $item//tgpnd:match_name/text() } ({ $item//tgpnd:preferred_name/text() })</a>
            </label>
        </div>,
        <div class="form-group">
			<input type="gndIdnew" placeholder="new GND ID" name="gndIdOverride" id="gndIdnew" class="form-control"/>
		</div>
    )
else (
    <p>The automatic search returned more than one value. The next items are the
    first provided by the search. Please confirm one of them or enter a correct 
    ID.</p>,
    <div class="form-group">
		<label>Select one of the following</label>
		{
            for $person at $pos in $item//tgpnd:person
            let $id := substring-after($person/tgpnd:link/@target, '/gnd/')
            return
                <div class="radio">
                    <input type="radio" value="{ $id }" id="gndId{ $pos }" name="gndIdConfirm"/>
                    <label for="gndId{ $pos }">
        				<a href="{ $person/tgpnd:link/string(@target) }" class="gndLink" target="_blank"> { $person/tgpnd:match_name/text() } ({ $person/tgpnd:preferred_name/text() }) </a> <small> { $person/tgpnd:info/text() } </small>
        			</label>
                </div>
        }
	</div>,
    <div class="form-group">
		<input type="gndIdnew" placeholder="new GND ID" id="gndIdnew" name="gndIdOverride" class="form-control"/>
	</div>
)
};

declare function kedit:updateDb( $doc, $para, $val ){
switch ($para)
case "gndIdConfirm"
    return
        let $persName :=  ($doc//tei:author)[1]
        return
            for $node in collection('/db/data/kolimo/header')//tei:author[ . = $persName ][parent::tei:titleStmt/parent::tei:fileDesc]
            let $test := console:log( $node/base-uri() )
            let $thisdoc := doc ( $node/base-uri() )
            let $oldValue := string($node/tei:persName/@ref)
            let $val := if(starts-with($val, 'http')) then substring-after($val, 'http://d-nb.info/gnd/') else $val
            return
                if( contains( string-join($node//text()), 'Unbekannt') or contains( string-join($node//text()), 'Anony') )
                then ()
                else
                    (
                    update value $node/tei:persName/@ref with 'http://d-nb.info/gnd/'||$val
                    ,
                    kedit:revision($thisdoc, $para, $val, $oldValue)
                )
default return ()
};

declare function kedit:revision($doc, $field, $value, $oldvalue){
    let $who := request:get-attribute("org.exist.login.user")
    let $name := sm:get-account-metadata($who, xs:anyURI('http://axschema.org/namePerson'))
    let $respStmt :=
                    if( $doc//tei:name/@xml:id = $who ) then () else
                        if( $doc//tei:respStmt[@xml:id = "kolimo-staff"] )
                            then <name xml:id="{$who}">{$name}</name>
                        else 
                            <respStmt xmlns="http://www.tei-c.org/ns/1.0" xml:id="kolimo-staff">
                                <resp>Erfassung der Metadaten</resp>
                                <name xml:id="{$who}">{$name}</name>
                            </respStmt>
    let $respStmtPlace := if( $doc//tei:respStmt[@xml:id="kolimo-staff"]  )
                            then 
                                $doc//tei:respStmt[@xml:id="textsource-1"]//tei:name 
                            else $doc//tei:respStmt[@xml:id="textsource-1"] 
    let $change :=
    <change xmlns="http://www.tei-c.org/ns/1.0"
    when="{current-dateTime()}"
    who="#{$who}">{upper-case($field)}&#160;{$oldvalue}&#160;{$value}</change>
    return
    (if(not(exists($doc//tei:revisionDesc))) 
    then update insert <revisionDesc xmlns="http://www.tei-c.org/ns/1.0"><listChange xmlns="http://www.tei-c.org/ns/1.0">{$change}</listChange></revisionDesc> into $doc//tei:teiHeader
    else update insert $change into $doc//tei:teiHeader/tei:revisionDesc/tei:listChange,
    update insert $respStmt following $respStmtPlace
    )
};