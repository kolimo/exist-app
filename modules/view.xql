(:~
 : This is the main XQuery which will (by default) be called by controller.xql
 : to process any URI ending with ".html". It receives the HTML from
 : the controller and passes it to the templating system.
 :)
xquery version "3.1";

import module namespace templates="http://exist-db.org/xquery/templates" ;

(: 
 : The following modules provide functions which will be called by the 
 : templating.
 :)
import module namespace config="http://kolimo.de/config" at "config.xqm";
import module namespace app="http://kolimo.de/templates" at "app.xql";
import module namespace kolimo="http://kolimo.de/" at "kolimo.xqm";
import module namespace ksearch="http://kolimo.uni-goettingen.de/ns/search" at "search.xqm";
import module namespace kedit="http://kolimo.uni-goettingen.de/ns/edit" at "edit.xqm";
import module namespace kolimo-fritz="http://kolimo.uni-goettingen.de/fritz" at "fritz.xqm";
import module namespace kolimo-markus="http://kolimo.uni-goettingen.de/markus" at "markus.xqm";
import module namespace kolimo-ronald="http://kolimo.uni-goettingen.de/ronald" at "ronald.xqm";
import module namespace kolimo-maxi="http://kolimo.uni-goettingen.de/maxi" at "maxi.xqm";
import module namespace kolimo-simone="http://kolimo.uni-goettingen.de/simone" at "simone.xqm";
import module namespace kolimo-berenike="http://kolimo.uni-goettingen.de/berenike" at "berenike.xqm";
import module namespace kolimo-mathias="http://kolimo.uni-goettingen.de/mathias"at "mathias.xqm";
import module namespace kolimotcf="http://kolimo.uni-goettingen.de/tcf" at "tcf.xqm";


import module namespace rede="http://kolimo.uni-goettingen.de/ns/rede" at "rede.xqm";


declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

declare option output:method "html5";
declare option output:media-type "text/html";

let $config := map {
    $templates:CONFIG_APP_ROOT := $config:app-root,
    $templates:CONFIG_STOP_ON_ERROR := true()
}
(:
 : We have to provide a lookup function to templates:apply to help it
 : find functions in the imported application modules. The templates
 : module cannot see the application modules, but the inline function
 : below does see them.
 :)
let $lookup := function($functionName as xs:string, $arity as xs:int) {
    try {
        function-lookup(xs:QName($functionName), $arity)
    } catch * {
        ()
    }
}
(:
 : The HTML is passed in the request from the controller.
 : Run it through the templating system and return the result.
 :)
let $content := request:get-data()
return
    templates:apply($content, $lookup, (), $config)