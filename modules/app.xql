xquery version "3.1";
(:~
 : This is the main KOLIMO router.
 : According to user rights all menus are generated and fully distrubted by this module
 : 
 : @author Mathias Göbel
 : @version 1.0
 : @see https://kolimo.uni-goettingen.de
 :)
module namespace app="http://kolimo.de/templates";

import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace templates="http://exist-db.org/xquery/templates" ;
import module namespace config="http://kolimo.de/config" at "config.xqm";


(:~
 : switch styles according to requested resource.
 : intension is to place site specific stylesheets in the html:head
 :)
declare function app:header-page-style($node as node(), $model as map(*)) {
let $uri := request:get-uri()
let $res := tokenize($uri, '/')[last()]
let $annoText := request:get-parameter('annoText', '')
let $annoUser := request:get-parameter('annoUser', '')
return
switch( $res )
case "gesamtliste.html"
    return
    (   <link rel="stylesheet" type="text/css" href="css/libs/bootstrap-editable.css" />,
	    <link rel="stylesheet" type="text/css" href="css/libs/dataTables.fixedHeader.css" />,
	    <link rel="stylesheet" type="text/css" href="css/libs/dataTables.tableTools.css" />,
	    <link rel="stylesheet" type="text/css" href="css/libs/select2.css" />
	)
	case "text.html" return
	    (if($annoText = "") then () else
	    <style>
	        {for $item in collection('/db/data/annotations/')//item[@type="object"]
	        where $annoText = $item//pair[@name="text"]/text()
            return
            '[data-annotation-id="' || $item//pair[@name = "id"]/string(.) || '"] {
            background-color: rgba(255,0,255,0.3) !important;}'
	        }
        </style>,
        if($annoUser = "") then () else
	    <style>
	        {for $item in collection('/db/data/annotations/')//item[@type="object"]
	        where $annoText = $item/ends-with(base-uri(), "_"||$annoUser||".xml")
            return
            '[data-annotation-id="' || $item//pair[@name = "id"]/string(.) || '"] {
            background-color: rgba(255,0,255,0.3) !important;}'
	        }
        </style>
        )
default return ()
};

(:~
 : switch scripts according to requested resource.
 : minimizes loading time for some sites, injects annotator and more on others
 :)
declare function app:footer-page-scripts($node as node(), $model as map(*)) {
let $uri := request:get-uri()
let $res := tokenize($uri, '/')[last()]
return
switch( $res )
case( "index.html" ) return (
    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot/excanvas.min.js"></script><![endif]-->,
	<script src="js/flot/jquery.flot.js"></script>,
	<script src="js/flot/jquery.flot.min.js"></script>,
	<script src="js/flot/jquery.flot.pie.min.js"></script>,
	<script src="js/flot/jquery.flot.stack.min.js"></script>,
	<script src="js/flot/jquery.flot.resize.min.js"></script>,
	<script src="js/flot/jquery.flot.time.min.js"></script>,
	<script src="js/flot/jquery.flot.orderBars.js"></script>,
	<script src="js/kolimo-index.js"></script>)
case "gesamtliste.html" return (
	<script src="js/bootstrap-editable.min.js"></script>,
	<script src="js/select2.min.js"></script>,
	<script src="js/moment.min.js"></script>,
	<script src="js/jquery.dataTables.js"></script>,
	<script src="js/dataTables.fixedHeader.js"></script>,
	<script src="js/dataTables.tableTools.js"></script>,
	<script src="js/jquery.dataTables.bootstrap.js"></script>,
	<script src="js/kolimo-gesamtliste.js"></script>)
case "gesamtliste2.html" return (
	<script src="js/bootstrap-editable.min.js"></script>,
	<script src="js/select2.min.js"></script>,
	<script src="js/moment.min.js"></script>,
	<script src="js/jquery.dataTables.js"></script>,
	<script src="js/dataTables.fixedHeader.js"></script>,
	<script src="js/dataTables.tableTools.js"></script>,
	<script src="js/jquery.dataTables.bootstrap.js"></script>,
	<script src="js/kolimo-gesamtliste2.js"></script>)
case "list.html" return (
	<script src="js/bootstrap-editable.min.js"></script>,
	<script src="js/select2.min.js"></script>,
	<script src="js/moment.min.js"></script>,
	<script src="js/jquery.dataTables.js"></script>,
	<script src="js/dataTables.fixedHeader.js"></script>,
	<script src="js/dataTables.tableTools.js"></script>,
	<script src="js/jquery.dataTables.bootstrap.js"></script>,
	<script src="js/kolimo-list.js"></script>)
case "browse.html" return (
    <script src="js/jquery.maskedinput.min.js"></script>,
    <script src="js/kolimo-browse.js"></script>)
case "text.html" return (
    <script src="js/annotator/annotator.min.js"></script>,
    <script src="js/kolimo-text.js"></script>
    )
case "textview.html" return (
    <script src="js/annotator/annotator.min.js"></script>,
    <script src="js/kolimo-textview.js"></script>
    )
default return
    if(substring-before($res, '.html')
        = xmldb:get-child-resources('/db/apps/kolimo//templates/js/')[starts-with(., 'kolimo-')]
            ! substring-after(substring-before(., '.'), 'kolimo-')) 
    then <script src="js/kolimo-{substring-before($res, '.html')}.js"></script>
    else ()
};

(:~
 : returns the complete menu in the navbar (top) according to login status
 :)
declare function app:menutop($node as node(), $model as map(*)){
if( xmldb:get-current-user() != "guest" ) then
<div class="nav-no-collapse pull-right" id="header-nav">
	<ul class="nav navbar-nav pull-right">
	    <li class="hidden-xs">
			<a class="btn" href="user.html">
				<i class="fa fa-cog"></i>
			</a>
		</li>
		<li class="hidden-xxs">
			<a class="btn" href="?logout=true">
				<i class="fa fa-power-off"></i>
			</a>
		</li>
	</ul>
</div>
else
<div class="nav-no-collapse pull-right" id="header-nav">
	<ul class="nav navbar-nav pull-right">
	    <li class="hidden-xs">
			<a class="btn" href="login.html">
				<i class="fa fa-sign-in"></i>
			</a>
		</li>
	</ul>
</div>
};

(:~
 : returns the complete menubar (left) including the userbox
 :)
declare function app:menuleft($node as node(), $model as map(*)){
<section id="col-left" class="col-left-nano">
    <div id="col-left-inner" class="col-left-nano-content">
        { local:userbox() }
        <div class="collapse navbar-collapse navbar-ex1-collapse" id="sidebar-nav">
            <ul class="nav nav-pills nav-stacked">
                <li>
                    <a href="about.html">
                        <i class="fa fa-info"/>
                        <span>About</span>
                    </a>
                </li>
                <li>
                    <a href="index.html">
                        <i class="fa fa-dashboard"/>
                        <span>Dashboard</span>
                        <span class="label label-info label-circle pull-right">0</span>
                    </a>
                </li>
                <li>
                    <a href="browse.html">
                        <i class="fa fa-table"/>
                        <span> Collection Browser </span>
                    </a>
                </li>
                <li>
                    <a href="rede.html">
                        <i class="fa fa-table"/>
                        <span> Redewiedergabe </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</section>
};

declare function local:userbox() {
<div id="user-left-box" class="clearfix hidden-sm hidden-xs">
    <!-- img alt="" src="img/samples/user.png"/ -->
    <div class="user-box">
        <span>
            <i style="margin-right:5px;" class="fa fa-user"></i>
            <span class="name">
    	        { xmldb:get-current-user() }
            </span>
        </span>
    </div>
</div>
};

(:~
 : modifes @html:action in a form
 : use it to remove a logout parameter (prevent a loop here) or to return to the HTTP:referer
 : stolen from the monex package
 :   :)
declare function app:form-action-to-current-url($node as node(), $model as map(*)) {
<form action="{ 
    substring-after(request:get-url(), '/kolimo/') ||
    (if(request:get-query-string() != '')
        then '?'||replace(request:get-query-string(), 'logout=true', 'removed=logout')
        else '') }">{
        $node/attribute()[not(name(.) = 'action')],
        $node/node()
    }
</form>
};

(:~
 : forwards requests that need rigths elevation to a securitywrapper called via localhost.
 : the securitywrapper should be accessible via localhost only and may contains a dba password in plain text.
 : every call should be evaluated from this script and the securitywrapper as well.
 : use with caution!
 : 
 : first part is a receiver function and calles out to the securitywrapper, second part delivers the results from the wrapper and the form.
 :)
declare function app:usersettings($node as node(), $model as map(*)){
let $username := xmldb:get-current-user()
let $test := console:log( $username )
(: test for email metadata and correct when possible :)
let $email :=   if( count(sm:get-account-metadata($username, xs:anyURI('http://axschema.org/contact/email')))=0) 
                then 
(:                    sm:set-account-metadata( :)
(:                        $username, :)
(:                        xs:anyURI('http://axschema.org/contact/email'), :)
(:                        sm:get-account-metadata($username, xs:anyURI('http://exist-db.org/security/description')) :)
(:                    ) :)
                    'Please enter a valid email adress.'
                else ()
let $url := 'http://localhost:8080/exist/apps/securitywrapper.xql?user='||$username||'&amp;'
let $write := if(request:get-parameter-names() = ()) then () else
                for $request in request:get-parameter-names()
                where request:get-parameter($request, '') != ''
                return
                    switch($request)
                        case 'password1' 
                            return 
                                if(request:get-parameter($request, '') = request:get-parameter('password0', '')) 
                                then 
                                    if(string-length( request:get-parameter($request, '') ) lt 8) 
                                    then 'Password to short. Use at least 8 characters.'
                                    else 
                                        let $do := httpclient:get( xs:anyURI( $url||'passwd='||encode-for-uri(request:get-parameter($request, '')) ), false(), ())
                                        return 'Password updated.'
                                else 'Passwords doesnt match. Nothing changed.'
                        case 'namePerson' return 
                                let $do := httpclient:get(xs:anyURI( $url||'namePerson='||encode-for-uri(request:get-parameter($request, '')) ), false(), ())
                                return 'Name changed.'
                        case 'email' return
                                let $do := httpclient:get(xs:anyURI( $url||'email='||encode-for-uri(request:get-parameter($request, ''))), false(), ())
                                return 'Email changed.'
                        default return ()
return
(for $item in $write return
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<i class="fa fa-info-circle fa-fw fa-lg"></i>
		<strong>Information: </strong> {$item}
	</div>
,
<form role="form" method="post">
<!-- p>request: {request:get-parameter-names()}</p>
<p>values: {request:get-parameter-names() ! (request:get-parameter(., 'empty') || ';')}</p -->
    <div class="form-group">
		<label for="InputnamePerson" title="(axschema.org/namePerson)">Name</label>
		<input
            class="form-control"
            name="namePerson"
            id="InputnamePerson"
            placeholder="{ sm:get-account-metadata($username, xs:anyURI('http://axschema.org/namePerson') ) }" 
            type="text" />
	</div>
    <div class="form-group">
		<label for="Inputemail" title="(axschema.org/namePerson)">E-Mail</label>
		<input
            class="form-control"
            name="email"
            id="Inputemail"
            placeholder="{ sm:get-account-metadata($username, xs:anyURI('http://axschema.org/contact/email') ) }" 
            type="email" />
	</div>
	<div class="form-group">
	    <label for="PwdMeter">Password</label>
	    <input type="password" name="password0" class="form-control" id="PwdMeter" placeholder="Enter password" data-indicator="pwindicator" />
    </div>
    <div class="form-group">
	    <label for="PwdMeter">Retype Password</label>
	    <input type="password" name="password1" class="form-control" id="PwdMeter" placeholder="Retype password" data-indicator="pwindicator" />
    	<div id="pwindicator" class="pwdindicator">
    		<div class="bar"></div>
    		<div class="pwdstrength-label"></div>
    	</div>
    </div>
    <button type="submit" class="btn btn-success">Submit</button>
</form>)
};

(:~
 : serves the error messages powered by programmingexcuses.com
 : a random message is created on each error report and send out to the user.
 :   :)
declare function app:error($node as node(), $model as map(*)) {
    httpclient:get(xs:anyURI('http://programmingexcuses.com/'), false(), ())//*:body/*:div[@class="wrapper"]/*:center/*:a/text()
};

(:~
 : returns  :)
declare function app:thisYear($node as node(), $model as map(*)) {
    year-from-date( current-date() )
};