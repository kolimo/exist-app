xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
import module namespace login="http://exist-db.org/xquery/login" at "resource:org/exist/xquery/modules/persistentlogin/login.xql";

declare function local:revision($doc, $field, $value, $oldvalue){
let $change := <change xmlns="http://www.tei-c.org/ns/1.0" when="{current-dateTime()}" who="{request:get-attribute("org.exist.login.user")}">{upper-case($field)}&#160;{$oldvalue}&#160;{$value}</change>
return
(if(not(exists($doc//tei:revisionDesc))) 
then update insert <revisionDesc xmlns="http://www.tei-c.org/ns/1.0"><listChange xmlns="http://www.tei-c.org/ns/1.0">{$change}</listChange></revisionDesc> into $doc//tei:teiHeader
else update insert $change into $doc//tei:teiHeader/tei:revisionDesc/tei:listChange
)
};

declare function local:author($old, $new){
    for $author in collection('/db/data/kolimo/NEWheader/')//tei:author
    where $author = $old
    return
        (update delete $author,
        update insert $new/tei:persName into $author)
};

declare function local:header($doc, $field, $value){
switch($field)
case "title" return (
                    local:revision($doc, $field, $value, $doc//tei:fileDesc[1]/tei:titleStmt[1]/tei:title[1]/string(.)),
                    update value $doc//tei:fileDesc[1]/tei:titleStmt[1]/tei:title[1] with $value
                    )
case "forename" return (
                    local:revision($doc, $field, $value, $doc//tei:fileDesc[1]/tei:titleStmt[1]/tei:author[1]/tei:persName[1]/tei:forename[1]/string(.)),
                    update value $doc//tei:fileDesc[1]/tei:titleStmt[1]/tei:author[1]/tei:persName[1]/tei:forename[1] with $value
                    )
case "surname" return (
                    local:revision($doc, $field, $value, $doc//tei:fileDesc[1]/tei:titleStmt[1]/tei:author[1]/tei:persName[1]/tei:surname[1]/string(.)),
                    if(exists($doc//tei:fileDesc[1]/tei:titleStmt[1]/tei:author[1]/tei:persName[1]/tei:surname[1])) then
                    update value $doc//tei:fileDesc[1]/tei:titleStmt[1]/tei:author[1]/tei:persName[1]/tei:surname[1] with $value
                    else
                        update insert <surname xmlns="http://www.tei-c.org/ns/1.0">{$value}</surname> into $doc//tei:fileDesc[1]/tei:titleStmt[1]/tei:author[1]/tei:persName[1]
                    )
case "firstPublication" return
                    let $original := $doc//tei:date[@type="firstPublication"]
                    return (
                    local:revision($doc, $field, $value, string($original/@when) ),
                    if(string($original) != "") then
                    update value $original with $value
                    else
                        update value $original with $value
                    )
default return false()
};

let $resource := request:get-parameter('resource', ''),
    $field := request:get-parameter('name', ''),
    $item :=  request:get-parameter('pk', ''),
    $value :=  request:get-parameter('value', '')

return
    switch($resource)
    case "header.post.html" return
        let $test := console:log("change header")
        let $docUri := collection('/db/data/kolimo/NEWheader/')//tei:idno[@type="kolimo"][. = $item]/base-uri()
        let $doc := doc($docUri)
        return
            local:header($doc, $field, $value)
    default return false()