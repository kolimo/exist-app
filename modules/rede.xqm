xquery version "3.1";
module namespace rede="http://kolimo.uni-goettingen.de/ns/rede";

import module namespace functx="http://www.functx.com" at "/db/system/repo/functx-1.0/functx/functx.xql";
declare namespace tei="http://www.tei-c.org/ns/1.0";


declare function rede:collection($node as node(), $model as map(*)) {

let $user := xmldb:get-current-user()

let $irre := doc( '/db/data/kolimo/header/tg/61718-Heym__Georg.xml' )
let $storch := doc( '/db/data/kolimo/header/gb/hauff/alma1826/alma1826.xml' )
let $entführung := doc( '/db/data/kolimo/header/tg/82947-Mus_us__Johann_Karl_August.xml' )

let $allDocs := (
    "/db/data/kolimo/header/dta/andreas_fenitschka_1898.TEI-P5.xml",
    "/db/data/kolimo/header/dta/dery_liebe_1896.TEI-P5.xml",
    "/db/data/kolimo/header/dta/fontane_irrungen_1888.TEI-P5.xml",
    "/db/data/kolimo/header/dta/kafka_hungerkuenstler_1922.TEI-P5.xml",
    "/db/data/kolimo/header/dta/kafka_urteil_1913.TEI-P5.xml",
    "/db/data/kolimo/header/dta/raabe_stopfkuchen_1891.TEI-P5.xml",
    "/db/data/kolimo/header/dta/storm_doppelgaenger_1887.TEI-P5.xml",
    "/db/data/kolimo/header/dta/eschstruth_katz_1886.TEI-P5.xml",
    "/db/data/kolimo/header/dta/hagenauer_muspilli_1900.TEI-P5.xml",
    "/db/data/kolimo/header/dta/kafka_prozess_1925.TEI-P5.xml",
    "/db/data/kolimo/header/dta/kafka_verwandlung_1915.TEI-P5.xml",
    "/db/data/kolimo/header/dta/keyserling_beatemareile_1903.TEI-P5.xml",
    "/db/data/kolimo/header/dta/may_kurdistan_1892.TEI-P5.xml",
    "/db/data/kolimo/header/dta/preuschen_yoshiwara_1920.TEI-P5.xml",
    "/db/data/kolimo/header/dta/spyri_heidi_1880.TEI-P5.xml",
    "/db/data/kolimo/header/dta/sturza_geluebde_1905.TEI-P5.xml",
    "/db/data/kolimo/header/dta/altenberg_prodromos_1906.TEI-P5.xml",
    "/db/data/kolimo/header/dta/reventlow_dames_1913.TEI-P5.xml",
    "/db/data/kolimo/header/dta/storm_riew_1885.TEI-P5.xml",
    "/db/data/kolimo/header/dta/kraft_medizinmann_1896.TEI-P5.xml",
    "/db/data/kolimo/header/dta/heyking_erzaehlungen_1918.TEI-P5.xml",
    "/db/data/kolimo/header/dta/keller_sinngedicht_1882.TEI-P5.xml",
    "/db/data/kolimo/header/dta/polenz_buettnerbauer_1895.TEI-P5.xml",
    "/db/data/kolimo/header/dta/fontane_briest_1896.TEI-P5.xml"
)

let $currentUsers := ("Thord","Wojciech","Nikolajus","Lis","Liutauras","Frerk","Fleur","Gundelinde","Koronos","Dilma","Gunnel", "X")

return (
for $text in ( $irre, $storch, $entführung )
let $id := $text//tei:idno[@type = "kolimo"]/string(.)
let $title := ($text//tei:title/text())[1]
return
    local:createBlock($id, $title, false())

    , <hr/>
    ,
    switch (xmldb:get-current-user())
        case "Thord" return     local:parseIdsToAnnotate( ( $allDocs[1], $allDocs[13], $allDocs[12], $allDocs[24]) )
        case "Wojciech" return  local:parseIdsToAnnotate( ( $allDocs[2], $allDocs[14], $allDocs[13], $allDocs[1]) )
        case "Nikolajus" return local:parseIdsToAnnotate( ( $allDocs[3], $allDocs[15], $allDocs[14], $allDocs[2]) )
        case "Lis" return       local:parseIdsToAnnotate( ( $allDocs[4], $allDocs[16], $allDocs[15], $allDocs[3]) )
        case "Liutauras" return local:parseIdsToAnnotate( ( $allDocs[5], $allDocs[17], $allDocs[16], $allDocs[4]) )
        case "Frerk" return     local:parseIdsToAnnotate( ( $allDocs[6], $allDocs[18], $allDocs[17], $allDocs[5]) )
        case "Fleur" return     local:parseIdsToAnnotate( ( $allDocs[7], $allDocs[19], $allDocs[18], $allDocs[6]) )
        case "Gundelinde"return local:parseIdsToAnnotate( ( $allDocs[8], $allDocs[20], $allDocs[19], $allDocs[7]) )
        case "Koronos" return   local:parseIdsToAnnotate( ( $allDocs[9], $allDocs[21], $allDocs[20], $allDocs[8]) )
        case "Dilma" return     local:parseIdsToAnnotate( ($allDocs[10], $allDocs[22], $allDocs[21], $allDocs[9]) )
        case "Gunnel" return    local:parseIdsToAnnotate( ($allDocs[11], $allDocs[23], $allDocs[22], $allDocs[10]) )
        case "Veselin" return   local:parseIdsToAnnotate( ($allDocs[12], $allDocs[24], $allDocs[23], $allDocs[11]) )
        case "Koronos" return ()
        case "x" return ()
        case "Jeremias" return ()
        case "Alvise" return ()
        case "Elke" return ()
        case "Yoko" return ()
        case "Protasius" return ()
        case "Prokop" return ()
        case "Bror" return ()
        case "Ubaldo" return ()
        default return local:parseIdsToAnnotate( $allDocs )
)};

declare function local:createBlock($id, $title, $short as xs:boolean) {
    <a class="no_underline" href="{if($short) then "textview.html" else "text.html"}?id={$id}"><div class="textbox">
        {$title}
    </div></a>
};
declare function local:parseIdsToAnnotate($seq) {
    for $item in $seq
    let $doc := doc($item)
    let $title := ($doc//tei:title//text())[1]
    let $id := ($doc//tei:idno/string(.))[1]
    return
        local:createBlock($id, $title, true())
};
declare function rede:text($node as node(), $model as map(*), $id as xs:string) {
    rede:textgrab($id)
};
declare function rede:textgrab($id as xs:string) {
    let $uri := collection( '/db/data/kolimo/header' )//tei:idno[. = $id]/base-uri()
    let $source := doc( replace( $uri, 'kolimo/header', 'kolimo/sources' ) )
    let $text := ($source//body/*, $source//tei:body/*)
    
    let $ps := ($text//*:p, $text//tei:l)
(:    let $ps := if($ps = ()) then $text//tei:l else $ps:)
    let $length := $ps[position() lt 150] ! functx:word-count( . )
    
    return
        for $i at $pos in $ps
        where sum( $length[position() lt $pos] ) lt 720
        return
            $i
};