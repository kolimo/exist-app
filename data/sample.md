# SAMPLE COLLECTION INFORMATION

ths app comes with sample data (10 documents from our sources) for development
purposes or to demonstrate basic features. there is no script, feature or
webinterface that operates on this data.
`post-install.xql` copies the `sample/*` collection to `/db/`
collection. everything else is done there!
