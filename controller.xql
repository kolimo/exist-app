xquery version "3.1";

import module namespace login="http://exist-db.org/xquery/login" at "resource:org/exist/xquery/modules/persistentlogin/login.xql";
import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";

declare variable $exist:path external;
declare variable $exist:resource external;
declare variable $exist:controller external;
declare variable $exist:prefix external;
declare variable $exist:root external;

if ($exist:path eq '') then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="{request:get-uri()}/"/>
    </dispatch>

else if ($exist:path eq "/") then
    (: forward root path to index.html :)
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <redirect url="index.html"/>
    </dispatch>
else if ($exist:resource = ('registration.html', 'forgot-password.html', 'login.html', 'imprint.html'
(:    , 'tcfview.html':)
    ) or starts-with($exist:resource, "workshop")) then
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <cache-control cache="yes"/>
        <forward url="{$exist:controller}/templates/{$exist:resource}"/>
    </dispatch>
else if (ends-with($exist:resource, ".xq")) then (
    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
        <cache-control cache="yes"/>
        <forward url="{$exist:controller}/{$exist:resource}"/>
    </dispatch>
    )
else if (not( ends-with($exist:resource, ".html") )) then
    (: everything else is passed through :)
        <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
            <cache-control cache="yes"/>
            <forward url="{$exist:controller}/templates/{$exist:path}"/>
        </dispatch>
else
    let $login := login:set-user("org.exist.login", (), false()),
        $user := request:get-attribute("org.exist.login.user"),
        $test := console:log( ($user, $exist:resource) )
        return
            if( starts-with($exist:resource, "workshop") or $exist:resource = ("index.html", "browse.html", "about.html", "text.html", "hello.html"))
            then
                <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                    <forward url="{$exist:controller}/templates/{$exist:resource}"/>
                    <view>
                        <forward url="{$exist:controller}/modules/view.xql">
                            <set-header name="Cache-Control" value="no-cache"/>
                        </forward>
                    </view>
            		<error-handler>
            			<forward url="{$exist:controller}/templates/error.html" method="get"/>
            			<forward url="{$exist:controller}/modules/view.xql"/>
            		</error-handler>
                </dispatch>
            else if(($exist:resource = "login.html") and ($user != ("", "guest"))) then (
                    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                        <redirect url="index.html"/>
                    </dispatch>)
            else
                (: the html page is run through view.xql to expand templates :)
                if ($user and sm:user-exists($user)) then
                        if($exist:resource = 'header.post.html') then
                                <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                                    <cache-control cache="no"/>
                                    <forward url="{$exist:controller}/modules/post.xql">
                                    <add-parameter name="resource" value="{$exist:resource}"/>
                                    </forward>
                                </dispatch>
                        else
                            <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                                <forward url="{$exist:controller}/templates/{$exist:resource}"/>
                                <view>
                                    <forward url="{$exist:controller}/modules/view.xql">
                                        <set-header name="Cache-Control" value="no-cache"/>
                                    </forward>
                                </view>
                        		<error-handler>
                        			<forward url="{$exist:controller}/templates/error.html" method="get"/>
                        			<forward url="{$exist:controller}/modules/view.xql"/>
                        		</error-handler>
                            </dispatch>
                else
                    <dispatch xmlns="http://exist.sourceforge.net/NS/exist">
                        <forward url="{$exist:controller}/templates/login.html"/>
                        <view>
                            <forward url="{$exist:controller}/modules/view.xql">
                                <set-header name="Cache-Control" value="no-cache"/>
                            </forward>
                        </view>
                    	<error-handler>
                    		<forward url="{$exist:controller}/templates/error.html" method="get"/>
                    		<forward url="{$exist:controller}/modules/view.xql"/>
                    	</error-handler>
                    </dispatch>
