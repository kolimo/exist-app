# KoLiMo Webapp

eXist-db App for KoLiMo sources.
Query and edit metadata of about 40k German language texts.

## build

    ant

you need to provide the `securitywrapper.xql` a simpe XQuery that evaluates incoming
POST data, login a dba user and performing tasks like changing user metadata (e.g. passwords)
this script should be placed behind the curtain and you are responsible to set up your http
server, so that this script is available via localhost only.

## installation

use eXists package installer

## sample data

this apps comes with sample data. from every source we have chosen 10 documents
randomly. they are placed in at `/db/data` - so dont edit anything inside the
`app.root/data/sample` folder.
